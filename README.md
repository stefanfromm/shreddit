```
        __                  __     __ __ __   
.-----.|  |--.----.-----.--|  |.--|  |__|  |_ 
|__ --||     |   _|  -__|  _  ||  _  |  |   _|
|_____||__|__|__| |_____|_____||_____|__|____|
```

**shreddit** is a tool for analyzing log files. It allows to chain several analysis steps into pipes and to store these pipe definitions permanently in a configuration file. You can define different pipes for each analysis use case. You can repeat analysis by just loading a configuration file and re-run the same analysis steps against another log file.

**shreddit** is more than just grep. It contains other analysis processors - a.k.a. "shredders" like cutting out date ranges, correlating events and search and replace.

**shreddit** can analyze log files in any size. It splits log files into log events and sends each log event through the pipe of analysis steps one by one. There are only buffered the log events needed to fulfill analysis, not the whole log file.

**shreddit** offers a help system for getting you started.

**shreddit** arised because of several reasons. I wanted to...

* play around with Scala. For sure the Scala code is very Java-like as I have a strong Java background.
* evaluate Scala combinator parsers for CLI.
* try Scala's XML support for persistence.
* setup a Scala project in Maven.
* create a text-based console UI. For this I used the https://github.com/mabe02/lanterna project.
* feel how the cake pattern works in real life.



# Build and run the demo

1. mvn -DskipTests clean package
1. cd demo
1. java -jar ..\target\shreddit-tool.jar
1. input "load" and press ENTER
1. input "run demo.log -> out.log" and press ENTER
1. inspect the file out.log in the demo directory



# Architecture

## Domain Model

`de.fromm.shreddit.pipe`, `de.fromm.shreddit.shredders`

The domain model consists of named pipes, which contain named steps in a well-defined order. Each step is associated with a shredder implementing an analysis.

```
             +------------------------------------------------------------+
             | pipe "P"                                                   |
+--------    |                                                            |    +-------- 
| ...  |_\   |    +-----------+       +-----------+       +-----------+   |    | ...  |_\
| .. ... | --|--> | step "1"  | ----> | step "2"  | ----> | step "3"  | --|--> | .. ... |
| .... . |   |    +-----------+       +-----------+       +-----------+   |    | .... . |
+--------+   |          |                   |                   |         |    +--------+
    in       |          v                   v                   v         |       out   
             |   +-------------+     +-------------+     +-------------+  |
             |   |  shredder   |     |  shredder   |     |  shredder   |  |
             |   |     e.g.    |     |     e.g.    |     |     e.g.    |  |
             |   | "daterange" |     | "correlate" |     |   "grep"    |  |
             |   +-------------+     +-------------+     +-------------+  |
             |                                                            |
             +------------------------------------------------------------+
```

The classes `Pipe` and `Step` represent pipes and steps. Shredders are represented by `Shredder` and its implementing subclasses. `Pipe.run()` starts a pipe against one or more input files. The input files are read one after each other, as if they were one file. At the output side there is created a new out file containing all analysis results. Shredders get initialized before starting and finalized after having finished.



## Input/Output Model

`de.fromm.shreddit.io`, `de.fromm.shreddit.lines`, `de.fromm.shreddit.events`

The input/output model has two abstraction layers. On the bottom layer files are represented as a sequence of lines (`Line`). A line consists of content and a line break. Lines are read via `LineIn` and written via `LineOut`.

On top of the line model there is the event model, which joins one or more lines to an event (`Event`). Events are read via `EventIn` and written via `EventOut`. shreddit allows to start a new log event when finding a defined marker in the content of a line.



## UI Shredder Model

`de.fromm.shreddit.cli`

At the UI layer pipes, steps and shredders have all an own representation. This is because the user is allowed to define constants which are resolved when using placeholders in shredder definitions. These constants are stored as given and are resolved only in the moment when a pipe is run. The UI shredder model is represented by `CliPipe`, `CliStep` and `CliShredder` and its subclasses.

The UI shredder model is transformed into the domain model when running a pipe (`CliPipe.toPipe()`).



## Persistence Model

`de.fromm.shreddit.persist`

Persistence is based on XML files. There is an internal XML model binding XML elements and attributes to classes. See `XmlPipe`, `XmlStep` and `XmlShredder` with its subclasses for the main entities of the XML model.

The XML classes offer methods for transforming XML classes to Scala XML nodes (`toXml()`) and back (`fromXml()`). Furthermore the XML classes offer methods for transforming XML classes to the UI model and back. The transformation chain looks like shown in the following diagram. The arrows are data flows.

```
 +---------------------------------+
 |     Cli[Pipe|Step|Shredder]     |
 +---------------------------------+
            |         ^
    toXml() |         | toCli()
            v         |
 +---------------------------------+
 |     Xml[Pipe|Step|Shredder]     |
 +---------------------------------+
            |         ^
   toNode() |         | fromNode()
            v         |
 +---------------------------------+
 |             Node                |
 +---------------------------------+
            |         ^
XML.write() |         | XML.loadFile()
            v         |
 +---------------------------------+
 |             File                |
 +---------------------------------+
```



## UI Command Model

`de.fromm.shreddit.cli`

The UI contains a command line where the user inputs commands. The first word is the command name followed by options and arguments. E.g. in

```
run demo.log -> out.log
```

`run` is the command name. For each command there is a subclass of `CliCommand`. `CliParser` is the parser for all of the supported commands. It is used in `CliWindow` to parse commands and execute them against the UI shredder model having the entry point `CliPipes` and against a `CliCallback` allowing commands to interact with the user.



## Services

`de.fromm.shreddit.cli`

There is implemented one service `StorageService` for XML. This service is realized by means of the CAKE pattern. `CliComponentRegistry` finally instantiates an `XmlStorageService` and makes it available for the UI layer.



## Help System

`de.fromm.shreddit.cli.help`

The online help system allows to get an overview by entering `help` and enter. There are many help topics which can be shown by entering `help <topic>`. Amongst others there is a help page for almost each command name. The help system can be easily extended by providing new help topic files in the package `de.fromm.shreddit.cli.help.topics`. The file name is the topic name used in the command input field.



## Known Issues

1. There is no input completion available.
1. Error handling is only rudimentary. There were made first steps to let the `CliParser` spit out more informative error messages, but it is still insufficient.
1. The command history is quite buggy.
1. There are missing commands for more convenience like e.g.
  * copyPipe, copyStep, moveStep, renamePipe, renameStep
  * md, mkdir
  * parseDate
  * resolve (constants)
1. You cannot run a shredder ad-hoc without defining a surrounding pipe and step.
