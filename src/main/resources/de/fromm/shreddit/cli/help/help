Common Commands
  help, ?                        - get help
  quit, close, exit, end, bye    - quit shreddit
  about                          - get technical information about this program
Managing Pipes
  addpipe, ap, newpipe, np       - define a new named pipe
  removepipe, rp, deletepipe, dp - discard an existing named pipe
  addstep, as, newstep, ns       - define a new step in the current pipe
  removestep, rs, deletestep, ds - discard an existing step
  use                            - select a pipe as the current pipe
  save                           - store all pipe definitions to a file
  load                           - load all pipe definitions from a file
  run                            - run a pipe against a text input
  pipes                          - show an overview of all defined pipes
  pipe                           - show details for a selected pipe
  events                         - sets event detection for the current pipe
Managing Constants
  const                          - show all currently defined constants
  set                            - define a constant
  unset                          - remove a constant
Miscellaneous
  hist                           - show the command history
  log                            - shows a log window
  env                            - shows the environment this program runs in
  cd                             - change the current working directory
  ls, dir                        - show the contents of the current working directory
  regex                          - test a regular expression with a text
  
For getting help for a single command, please input:

  help <commandname>
  
where the command name is the first mentioned command name from a line below.

There is a set of common help topics not being backed by commands. They are
shown by:

  help <topicname>

The following help topics are available:

  regex      - how to define regular expressions
  constants  - how to work with constants
  dateformat - how to define date format patterns
