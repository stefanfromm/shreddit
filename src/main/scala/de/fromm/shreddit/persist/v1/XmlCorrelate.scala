package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.CliCorrelate
import de.fromm.shreddit.cli.CliShredder
import scala.xml.Node

/**
 * @author Stefan Fromm
 */
abstract class XmlCorrelate extends XmlShredder {
  val id = "correlate"
  val startPattern: String
  val endPattern: String
  val cancelPattern: Option[String]
  val dateExtractionPattern: Option[String]
  val dateFormat: String
  val maxTimeLimit: Option[Long]

  def toNode() =
    <correlate startPattern={ startPattern } endPattern={ endPattern } cancelPattern={ cancelPattern match { case Some(p) => p; case _ => "" } } dateExtractionPattern={ dateExtractionPattern match { case Some(p) => p; case _ => "" } } dateFormat={ dateFormat } maxTimeLimit={ maxTimeLimit match { case Some(l) => l.toString; case _ => "" } }/>

  def toCli() = {
    val correlate = new CliCorrelate
    correlate.startPattern = startPattern
    correlate.endPattern = endPattern
    correlate.cancelPattern = cancelPattern
    correlate.dateExtractionPattern = dateExtractionPattern
    correlate.dateFormat = dateFormat
    correlate.maxTimeLimit = maxTimeLimit
    correlate
  }
}

/**
 * @author Stefan Fromm
 */
object XmlCorrelate {
  val factory =
    new XmlShredderFactory[XmlCorrelate] {
      def supports(cliShredder: CliShredder): Boolean =
        "correlate" == cliShredder.id

      protected def toXml0(cliShredder: CliShredder): XmlShredder = {
        val cliCorrelate = cliShredder.asInstanceOf[CliCorrelate]
        new XmlCorrelate {
          val startPattern = cliCorrelate.startPattern
          val endPattern = cliCorrelate.endPattern
          val cancelPattern = cliCorrelate.cancelPattern
          val dateExtractionPattern = cliCorrelate.dateExtractionPattern
          val dateFormat = cliCorrelate.dateFormat
          val maxTimeLimit = cliCorrelate.maxTimeLimit
        }
      }

      def supports(node: Node): Boolean =
        "correlate" == node.label

      protected def fromNode0(node: Node): XmlCorrelate =
        new XmlCorrelate {
          val startPattern = (node \ "@startPattern").text
          val endPattern = (node \ "@endPattern").text
          val cancelPattern = (node \ "@cancelPattern").text match {
            case "" => None
            case cp => Some(cp)
          }
          val dateExtractionPattern = (node \ "@dateExtractionPattern").text match {
            case "" => None
            case xp => Some(xp)
          } 
          val dateFormat = (node \ "@dateFormat").text
          val maxTimeLimit = (node \ "@maxTimeLimit").text match {
            case "" => None
            case r => Some(r.toLong)
          }
        }
    }
}