package de.fromm.shreddit.persist.v1

import scala.xml.Node
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.OneToOneEventBoundaries

/**
 * docme: XmlOneToOneEventBoundaries
 *
 * @author Stefan Fromm
 */
abstract class XmlOneToOneEventBoundaries extends XmlEventBoundaries {
  def toNode(): Node =
    <line/>

  def toBoundaries(): EventBoundaries =
    new OneToOneEventBoundaries
}

/**
 * @author Stefan Fromm
 */
object XmlOneToOneEventBoundaries {
  val factory = new XmlEventBoundariesFactory[XmlOneToOneEventBoundaries] {
    def supports(boundaries: EventBoundaries): Boolean =
      boundaries.isInstanceOf[OneToOneEventBoundaries]

    protected def toXml0(boundaries: EventBoundaries): XmlEventBoundaries =
      new XmlOneToOneEventBoundaries {
      }

    def supports(node: Node): Boolean =
      "line" == node.label

    protected def fromXml0(node: Node): XmlOneToOneEventBoundaries =
      new XmlOneToOneEventBoundaries {
      }
  }
}