package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.CliRewrite
import de.fromm.shreddit.cli.CliShredder
import scala.xml.Node

/**
 * docme: XmlRewrite
 *
 * @author Stefan Fromm
 */
abstract class XmlRewrite extends XmlShredder {
  val id = "rewrite"
  val detectionPattern: Option[String]
  val replacePattern: String
  val replacement: Option[String]

  def toNode() =
    <rewrite detectionPattern={ if (detectionPattern.isDefined) detectionPattern.get else "" } replacePattern={ replacePattern } replacement={ if (replacement.isDefined) replacement.get else "" }/>

  def toCli() = {
    val rewrite = new CliRewrite
    rewrite.detectionPattern = detectionPattern
    rewrite.replacePattern = replacePattern
    rewrite.replacement = replacement
    rewrite
  }
}

/**
 * @author Stefan Fromm
 */
object XmlRewrite {
  val factory =
    new XmlShredderFactory[XmlRewrite] {
      def supports(cliShredder: CliShredder): Boolean =
        "rewrite" == cliShredder.id

      protected def toXml0(cliShredder: CliShredder): XmlShredder = {
        val cliRewrite = cliShredder.asInstanceOf[CliRewrite]
        new XmlRewrite {
          val detectionPattern = cliRewrite.detectionPattern
          val replacePattern = cliRewrite.replacePattern
          val replacement = cliRewrite.replacement
        }
      }

      def supports(node: Node): Boolean =
        "rewrite" == node.label

      protected def fromNode0(node: Node): XmlRewrite =
        new XmlRewrite {
          val detectionPattern = (node \ "@detectionPattern").text match {
            case "" => None
            case dp => Some(dp)
          }
          val replacePattern = (node \ "@replacePattern").text
          val replacement = (node \ "@replacement").text match {
            case "" => None
            case r => Some(r)
          }
        }
    }
}