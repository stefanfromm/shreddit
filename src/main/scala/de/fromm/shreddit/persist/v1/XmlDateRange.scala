package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.CliDateRange
import de.fromm.shreddit.cli.CliShredder
import scala.xml.Node

/**
 * @author Stefan Fromm
 */
abstract class XmlDateRange extends XmlShredder {
  val id = "daterange"
  val dateExtractionPattern: String
  val dateFormat: String
  val from: Option[String]
  val until: Option[String]

  def toNode() =
    <daterange dateExtractionPattern={ dateExtractionPattern } dateFormat={ dateFormat } from={ from match { case Some(f) => f; case _ => "" } } until={ until match { case Some(u) => u; case _ => "" } }/>

  def toCli() = {
    val daterange = new CliDateRange
    daterange.dateExtractionPattern = dateExtractionPattern
    daterange.dateFormat = dateFormat
    daterange.from = from
    daterange.until = until
    daterange
  }
}

/**
 * @author Stefan Fromm
 */
object XmlDateRange {
  val factory =
    new XmlShredderFactory[XmlDateRange] {
      def supports(cliShredder: CliShredder): Boolean =
        "daterange" == cliShredder.id

      protected def toXml0(cliShredder: CliShredder): XmlShredder = {
        val cliDateRange = cliShredder.asInstanceOf[CliDateRange]
        new XmlDateRange {
          val dateExtractionPattern = cliDateRange.dateExtractionPattern
          val dateFormat = cliDateRange.dateFormat
          val from = cliDateRange.from
          val until = cliDateRange.until
        }
      }

      def supports(node: Node): Boolean =
        "daterange" == node.label

      protected def fromNode0(node: Node): XmlDateRange =
        new XmlDateRange {
          val dateExtractionPattern = (node \ "@dateExtractionPattern").text
          val dateFormat = (node \ "@dateFormat").text
          val from = (node \ "@from").text match {
            case "" => None
            case f => Some(f)
          }
          val until = (node \ "@until").text match {
            case "" => None
            case u => Some(u)
          }
        }
    }
}