package de.fromm.shreddit.persist.v1

import scala.xml.Node
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries

/**
 * docme: XmlEventBoundaries
 *
 * @author Stefan Fromm
 */
abstract class XmlEventBoundaries {
  def toNode(): Node

  def toBoundaries(): EventBoundaries
}

/**
 * @author Stefan Fromm
 */
abstract class XmlEventBoundariesFactory[B <: XmlEventBoundaries] {
  def supports(boundaries: EventBoundaries): Boolean

  def supports(node: Node): Boolean

  def fromXml(node: Node): Option[B] = {
    if (supports(node))
      Some(fromXml0(node))
    else
      None
  }

  protected def fromXml0(node: Node): B

  def toXml(boundaries: EventBoundaries): Option[XmlEventBoundaries] = {
    if (supports(boundaries))
      Some(toXml0(boundaries))
    else
      None
  }

  protected def toXml0(boundaries: EventBoundaries): XmlEventBoundaries
}

/**
 * @author Stefan Fromm
 */
object XmlEventBoundaries {
  private val factories = List(XmlOneToOneEventBoundaries.factory, XmlMarkerEventBoundaries.factory)

  def toXml(boundaries: EventBoundaries) = factories.find(_.supports(boundaries)) match {
    case Some(f) => f.toXml(boundaries).get
    case None => throw new IllegalArgumentException("Event boundaries %s not supported.".format(boundaries.getClass.getSimpleName))
  }

  def fromXml(node: Node): XmlEventBoundaries = factories.find(_.supports(node)) match {
    case Some(f) => f.fromXml(node).get
    case None => throw new IllegalArgumentException("Node %s not supported.".format(node.label))
  }
}
