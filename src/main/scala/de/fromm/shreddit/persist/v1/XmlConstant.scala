package de.fromm.shreddit.persist.v1

import scala.xml.Node
import de.fromm.shreddit.cli.CliPipes

/**
 * docme: XmlConstant
 *
 * @author Stefan Fromm
 */
abstract class XmlConstant {
  val name: String
  val value: String

  def toNode() =
    <const name={ name } value={ value }/>
    
  def toCli(cliPipes: CliPipes) {
    cliPipes.setConstant(name, value)
  }
}

/**
 * @author Stefan Fromm
 */
object XmlConstant {
  def toXml(constant: (String, String)) = {
    new XmlConstant {
      val name = constant._1
      val value = constant._2
    }
  }

  def fromNode(node: Node): XmlConstant = {
    new XmlConstant {
      val name = (node \ "@name").text
      val value = (node \ "@value").text
    }
  }
}