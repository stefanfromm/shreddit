package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.CliStep
import scala.xml.Node
import de.fromm.shreddit.cli.CliShredder

/**
 * docme: XmlShredder
 *
 * @author Stefan Fromm
 */
abstract class XmlShredder {
  val id: String

  def toNode(): Node

  def toCli(): CliShredder
}

/**
 * @author Stefan Fromm
 */
abstract class XmlShredderFactory[P <: XmlShredder] {
  def supports(cliShredder: CliShredder): Boolean

  def supports(node: Node): Boolean

  def fromNode(node: Node): Option[P] = {
    if (supports(node))
      Some(fromNode0(node))
    else
      None
  }

  protected def fromNode0(node: Node): P

  def toXml(cliShredder: CliShredder): Option[XmlShredder] = {
    if (supports(cliShredder))
      Some(toXml0(cliShredder))
    else
      None
  }

  protected def toXml0(cliShredder: CliShredder): XmlShredder
}

/**
 * @author Stefan Fromm
 */
object XmlShredder {
  private val factories = List(XmlCorrelate.factory, XmlDateRange.factory, XmlGrep.factory, XmlRewrite.factory, XmlVolume.factory)

  def toXml(cliShredder: CliShredder) = factories.find(_.supports(cliShredder)) match {
    case Some(f) => f.toXml(cliShredder).get
    case None => throw new IllegalArgumentException("Shredder %s not supported.".format(cliShredder.id))
  }

  def fromNode(node: Node): XmlShredder = factories.find(_.supports(node)) match {
    case Some(f) => f.fromNode(node).get
    case None => throw new IllegalArgumentException("Node %s not supported.".format(node.label))
  }
}