package de.fromm.shreddit.persist.v1

import scala.xml.Node
import scala.util.matching.Regex
import de.fromm.shreddit.cli.CliShredder
import de.fromm.shreddit.cli.CliGrep

/**
 * docme: XmlGrep
 *
 * @author Stefan Fromm
 */
abstract class XmlGrep extends XmlShredder {
  val id = "grep"
  val pattern: String
  val before: Int
  val after: Int
  val reverse: Boolean

  def toNode() =
    <grep pattern={ pattern } before={ before.toString } after={ after.toString } reverse={ reverse.toString }/>

  def toCli() = {
    val grep = new CliGrep
    grep.pattern = pattern
    grep.before = before
    grep.after = after
    grep.reverse = reverse
    grep
  }
}

/**
 * @author Stefan Fromm
 */
object XmlGrep {
  val factory =
    new XmlShredderFactory[XmlGrep] {
      def supports(cliShredder: CliShredder): Boolean =
        "grep" == cliShredder.id

      protected def toXml0(cliShredder: CliShredder): XmlShredder = {
        val cliGrep = cliShredder.asInstanceOf[CliGrep]
        new XmlGrep {
          val pattern = cliGrep.pattern
          val before = cliGrep.before
          val after = cliGrep.after
          val reverse = cliGrep.reverse
        }
      }

      def supports(node: Node): Boolean =
        "grep" == node.label

      protected def fromNode0(node: Node): XmlGrep =
        new XmlGrep {
          val pattern = (node \ "@pattern").text
          val before = (node \ "@before").text.toInt
          val after = (node \ "@after").text.toInt
          val reverse = (node \ "@reverse").text.toBoolean
        }
    }
}