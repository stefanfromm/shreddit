package de.fromm.shreddit.persist.v1

import scala.xml.Node
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.MarkerEventBoundaries
import de.fromm.shreddit.pipe.MarkerEventBoundaries

/**
 * docme: XmlMarkerEventBoundaries
 *
 * @author Stefan Fromm
 */
abstract class XmlMarkerEventBoundaries extends XmlEventBoundaries {
  val startPattern: String
  val maxLength: Int

  def toNode(): Node =
    <marker startPattern={ startPattern } maxLength={ maxLength.toString }/>

  def toBoundaries(): EventBoundaries =
    new MarkerEventBoundaries(startPattern, maxLength)
}

/**
 * @author Stefan Fromm
 */
object XmlMarkerEventBoundaries {
  val factory = new XmlEventBoundariesFactory[XmlMarkerEventBoundaries] {
    def supports(boundaries: EventBoundaries): Boolean =
      boundaries.isInstanceOf[MarkerEventBoundaries]

    protected def toXml0(boundaries: EventBoundaries): XmlEventBoundaries = {
      val marker = boundaries.asInstanceOf[MarkerEventBoundaries]
      new XmlMarkerEventBoundaries {
        val startPattern = marker.startPattern
        val maxLength = marker.maxEventLength
      }
    }

    def supports(node: Node): Boolean =
      "marker" == node.label

    protected def fromXml0(node: Node): XmlMarkerEventBoundaries =
      new XmlMarkerEventBoundaries {
        val startPattern = (node \ "@startPattern").text
        val maxLength = (node \ "@maxLength").text.toInt
      }
  }
}