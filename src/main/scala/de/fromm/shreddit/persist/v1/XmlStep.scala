package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.CliStep
import scala.xml.Node
import de.fromm.shreddit.cli.CliPipe

/**
 * docme: XmlStep
 *
 * @author Stefan Fromm
 */
abstract class XmlStep {
  val name: String
  val shredder: XmlShredder

  def toNode() =
    <step name={ name }>{ shredder.toNode() }</step>

  def toCli(cliPipe: CliPipe) {
    cliPipe.addStep(name, shredder.toCli) //todo: handle case of non-unique name
    //todo: grep, etc
  }
}

/**
 * @author Stefan Fromm
 */
object XmlStep {
  def toXmlStep(cliStep: CliStep) = {
    new XmlStep {
      val name = cliStep.name
      val shredder = XmlShredder.toXml(cliStep.shredder)
    }
  }

  def fromXml(node: Node): XmlStep = {
    new XmlStep {
      val name = (node \ "@name").text
      val shredder = XmlShredder.fromNode((node \ "_")(0))
    }
  }
}