package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.CliPipe
import scala.xml.Node
import de.fromm.shreddit.cli.CliPipes
import scala.xml.Elem

/**
 * docme: XmlPipe
 *
 * @author Stefan Fromm
 */
abstract class XmlPipe {
  val name: String
  val steps: List[XmlStep]
  val eventBoundaries: XmlEventBoundaries

  def toNode() =
    <pipe name={ name }>{ if (!steps.isEmpty) { <steps>{ steps.map(_.toNode) }</steps> } }<eventBoundaries>{ eventBoundaries.toNode }</eventBoundaries></pipe>

  def toCli(cliPipes: CliPipes) {
    val cliPipe = cliPipes.addPipe(name).get //todo: handle case of non-unique or empty name
    steps.foreach(_.toCli(cliPipe))
    cliPipe.setEventBoundaries(eventBoundaries.toBoundaries)
  }
}

/**
 * @author Stefan Fromm
 */
object XmlPipe {
  def toXml(cliPipe: CliPipe) = {
    new XmlPipe {
      val name = cliPipe.name
      val steps = cliPipe.steps.map(XmlStep.toXmlStep(_))
      val eventBoundaries = XmlEventBoundaries.toXml(cliPipe.eventBoundaries)
    }
  }

  def fromNode(node: Node): XmlPipe = {
    new XmlPipe {
      val name = (node \ "@name").text
      //improveme: elemName as closure
      val steps = (node \ "steps" \ "step").map(XmlStep.fromXml(_)).toList //todo: handle non-existing steps
      val eventBoundaries = (node \ "eventBoundaries" \ "_") match {
        case Seq(n) => XmlEventBoundaries.fromXml(n)
        case _ => new XmlOneToOneEventBoundaries {} // fallback if not present yet in XML
      }
    }
  }
}