package de.fromm.shreddit.persist.v1

import scala.xml.Node
import scala.util.matching.Regex
import de.fromm.shreddit.cli.CliShredder
import de.fromm.shreddit.cli.CliGrep
import de.fromm.shreddit.cli.CliVolume

/**
 * docme: XmlGrep
 *
 * @author Stefan Fromm
 */
abstract class XmlVolume extends XmlShredder {
  val id = "volume"
  val categoryPatterns: List[String]

  def toNode() =
    <volume>
      {
        categoryPatterns.map(cp => {
          <category>{ cp }</category>
        })
      }
    </volume>

  def toCli() = {
    val volume = new CliVolume
    volume.categoryPatterns = categoryPatterns
    volume
  }
}

/**
 * @author Stefan Fromm
 */
object XmlVolume {
  val factory =
    new XmlShredderFactory[XmlVolume] {
      def supports(cliShredder: CliShredder): Boolean =
        "volume" == cliShredder.id

      protected def toXml0(cliShredder: CliShredder): XmlShredder = {
        val cliVolume = cliShredder.asInstanceOf[CliVolume]
        new XmlVolume {
          val categoryPatterns = cliVolume.categoryPatterns
        }
      }

      def supports(node: Node): Boolean =
        "volume" == node.label

      protected def fromNode0(node: Node): XmlVolume =
        new XmlVolume {
          val categoryPatterns = (node \ "category").map(_.text).toList
        }
    }
}