package de.fromm.shreddit.persist.v1

import scala.xml.Node
import de.fromm.shreddit.cli.CliPipes

/**
 * docme: XmlPipes
 *
 * @author Stefan Fromm
 */
abstract class XmlPipes {
  val version: Int
  val constants: List[XmlConstant]
  val pipes: List[XmlPipe]
  val currentPipeName: Option[String]

  def toNode() =
    <shreddit version={ version.toString }>
      <constants>{ constants.map(_.toNode()) }</constants>
      <pipes current={ if (currentPipeName.isDefined) currentPipeName.get else "" }>{ pipes.map(_.toNode()) }</pipes>
    </shreddit>

  def toCli(cliPipes: CliPipes) {
    pipes.foreach(_.toCli(cliPipes))
    constants.foreach(_.toCli(cliPipes))
    if (currentPipeName.isDefined)
      cliPipes.currentPipe = currentPipeName.get
  }
}

/**
 * @author Stefan Fromm
 */
object XmlPipes {
  def fromNode(node: Node): XmlPipes = {
    new XmlPipes {
      val version = (node \ "@version").text.toInt
      val constants = (node \ "constants" \ "const").map(XmlConstant.fromNode(_)).toList
      val pipes = (node \ "pipes" \ "pipe").map(XmlPipe.fromNode(_)).toList
      val currentPipeName = (node \ "pipes" \ "@current").text match {
        case "" => None
        case t => Some(t)
      }

      println((node \ "pipes" \ "@current").text)
    }
  }
}