package de.fromm.shreddit.persist.v1

/**
 * docme: XmlCommand
 *
 * @author Stefan Fromm
 */
abstract class XmlCommand {
  val cmdLine: String
}