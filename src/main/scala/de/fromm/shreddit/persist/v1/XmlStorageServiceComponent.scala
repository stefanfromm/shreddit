package de.fromm.shreddit.persist.v1

import de.fromm.shreddit.cli.StorageServiceComponent
import de.fromm.shreddit.cli.CliPipes
import scala.xml.XML
import java.io.File
import java.io.FileOutputStream
import java.nio.channels.Channels
import java.nio.charset.Charset
import scala.util.control.Exception

/**
 * docme: StorageServiceImpl
 *
 * @author Stefan Fromm
 */
trait XmlStorageServiceComponent extends StorageServiceComponent {
  /**
   * @author Stefan Fromm
   */
  class XmlStorageService extends StorageService {
    private def getFile(fo: Option[File]): File =
      (fo match {
        case Some(f) => f
        case None => new File("shreddit.xml")
      }).getCanonicalFile

    def loadPipes(source: Option[File]): CliPipes = {
      val cliPipes = new CliPipes
      XmlPipes.fromNode(XML.loadFile(getFile(source))).toCli(cliPipes)
      cliPipes
    }

    def savePipes(p: CliPipes, target: Option[File]) {
      //todo: make target file name selectable
      //todo: ask for overwriting an existing file
      val enc = Charset.defaultCharset.name
      val fos = new FileOutputStream(getFile(target))
      val w = Channels.newWriter(fos.getChannel(), enc)

      val node = new XmlPipes {
        val version = 1
        val pipes = p.pipes.map(XmlPipe.toXml(_))
        val constants = p.constants.toList.map(XmlConstant.toXml(_))
        val currentPipeName = p.currentPipe.map(_.name)
      }.toNode

      Exception.ultimately(w.close())(
        XML.write(w, node, enc, false, null))
    }
  }
}