package de.fromm.shreddit.lines

/**
 * @author Stefan Fromm
 */
protected[lines] class DefaultLine(val content: String) extends Line {
  def lineBreak = LineBreak.Auto
}