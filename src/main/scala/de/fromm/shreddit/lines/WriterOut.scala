package de.fromm.shreddit.lines

import java.io.Writer

/**
 * @author Stefan Fromm
 */
class WriterOut(private val writer: Writer) extends LineOut {
  require(writer != null)

  def close {
    writer.close
  }

  def write(line: Line) {
    writer.write(line.content)
    writer.write(line.lineBreak.value)
  }
}