package de.fromm.shreddit.lines

/**
 * @author Stefan Fromm
 */
object SysOut extends LineOut {
  def write(line: Line) {
    println(line.content)
  }

  def close {
  }
}
