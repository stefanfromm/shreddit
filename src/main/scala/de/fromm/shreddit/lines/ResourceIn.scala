package de.fromm.shreddit.lines

import java.io.InputStreamReader

/**
 * @author Stefan Fromm
 */
class ResourceIn(url: String) extends LineIn {
  private val _readerIn = getClass.getResourceAsStream(url) match {
    case null => null.asInstanceOf[ReaderIn]
    case is => new ReaderIn(new InputStreamReader(is)) //todo: choose charset
  }

  def read =
    if (_readerIn != null)
      _readerIn.read
    else
      None

  def close {
    if (_readerIn != null)
      _readerIn.close
  }
}
