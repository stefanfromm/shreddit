package de.fromm.shreddit.lines

import java.io.StringReader

/**
 * @author Stefan Fromm
 */
class StringIn(s: String) extends LineIn {
  private val _readerIn = new ReaderIn(new StringReader(s))

  def read: Option[Line] =
    _readerIn.read

  def close {
    _readerIn.close
  }
}
