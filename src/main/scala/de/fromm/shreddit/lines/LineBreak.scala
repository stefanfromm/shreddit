package de.fromm.shreddit.lines

import de.fromm.shreddit.util.Enum

/**
 * docme: LineBreak
 *
 * @author Stefan Fromm
 */
sealed trait LineBreak extends LineBreak.Value {
  def value: String
  def name: String

  override def toString = value
}

/**
 * docme: LineBreak$
 *
 * @author Stefan Fromm
 */
object LineBreak extends Enum[LineBreak] {
  /**
   * @author Stefan Fromm
   */
  case object Mac extends LineBreak {
    val value = "\r"
    val name = "Mac"
  }; Mac
  
  /**
   * @author Stefan Fromm
   */
  case object Unix extends LineBreak {
    val value = "\n"
    val name = "Unix"
  }; Unix
  
  /**
   * @author Stefan Fromm
   */
  case object Win extends LineBreak {
    val value = "\r\n"
    val name = "Windows"
  }; Win
  
  /**
   * @author Stefan Fromm
   */
  case object Auto extends LineBreak {
    val value = System.getProperty("line.separator")
    val name = "Auto"
  }; Auto
  
  /**
   * @author Stefan Fromm
   */
  case object None extends LineBreak {
    val value = ""
    val name = "None"
  }; None
  

  def detect(s: String): LineBreak = {
    for (lb <- values)
      if (lb.value == s)
        return lb

    None
  }
}