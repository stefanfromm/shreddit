package de.fromm.shreddit.lines

/**
 * docme: MultiLineIn
 *
 * @author Stefan Fromm
 */
class MultiLineIn(val ins: List[LineIn]) extends LineIn {
  require(ins != null)

  private var _rest: List[LineIn] = ins

  def read: Option[Line] = {
    _rest match {
      case Nil => None
      case head :: tail => {
        head.read match {
          case Some(line) => Some(line)
          case None => {
            _rest = tail
            read
          }
        }
      }
    }
  }

  override def estimateVolume: Long =
    ins.foldLeft(0L)((sum, in) => sum + in.estimateVolume)

  def close {
    ins.foreach(_.close) //todo: handle errors: close as many as possible
  }
}