package de.fromm.shreddit.lines

import java.io.BufferedReader
import java.io.Reader

/**
 * @author Stefan Fromm
 */
class ReaderIn(reader: Reader) extends LineIn {
  require(reader != null)

  private val _bufferedReader = if (reader.isInstanceOf[BufferedReader]) reader.asInstanceOf[BufferedReader] else new BufferedReader(reader)

  def read: Option[Line] = {
    val next = _bufferedReader.readLine

    if (next == null)
      None
    else
      Some(new Line {
        def content = next

        def lineBreak = LineBreak.Auto //todo: detect real line break
      })
  }

  def close {
    _bufferedReader.close
  }
}

