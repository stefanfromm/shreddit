package de.fromm.shreddit.lines

import scala.io.StdIn

/**
 * @author Stefan Fromm
 */
object SysIn extends LineIn {
  def read = {
    val next = StdIn.readLine
    if (next == null)
      None
    else
      Some(new DefaultLine(next))
  }

  def close {
  }
}
