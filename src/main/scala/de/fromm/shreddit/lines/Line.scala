package de.fromm.shreddit.lines

/**
 * docme: Line
 *
 * @author Stefan Fromm
 */
trait Line {
  def content: String

  def lineBreak: LineBreak

  def volume: Long =
    content.length + lineBreak.value.length

  override def toString = content
}

/**
 * @author Stefan Fromm
 */
object Line {
  def apply(c: String): Line =
    apply(c, LineBreak.Auto)

  def apply(c: String, lb: LineBreak) = new Line {
    val content = c
    val lineBreak = lb
  }
}