package de.fromm.shreddit.lines

import java.io.BufferedReader
import java.io.StringReader
import java.io.InputStreamReader
import java.io.Reader
import scala.io.StdIn

/**
 * docme: LineIn
 *
 * @author Stefan Fromm
 */
trait LineIn extends Iterable[Line] {
  override def iterator: Iterator[Line] = new Iterator[Line] {
    private var _next: Option[Line] = read //improveme: make it more lazy and read first time, if next() or hasNext is called

    def next(): Line = {
      val current = _next
      _next = read

      current match {
        case Some(l) => l
        case None => throw new NoSuchElementException
      }
    }

    def hasNext: Boolean =
      _next.isDefined
  }

  def estimateVolume: Long =
    0L

  def read: Option[Line]

  def close
}
