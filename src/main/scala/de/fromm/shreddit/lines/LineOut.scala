package de.fromm.shreddit.lines

import java.io.Writer

/**
 * docme: LineOut
 *
 * @author Stefan Fromm
 */
trait LineOut {
  def write(line: Line)

  def close
}
