package de.fromm.shreddit.events

import de.fromm.shreddit.lines.Line
import de.fromm.shreddit.lines.LineBreak

/**
 * An event having been extracted from a log file or another log source. A log event is immutable, this means its
 * represented content does not change. This log event is thread-safe.
 *
 * @author Stefan Fromm
 */
trait Event {
  @volatile
  private var _contentCache: String = null

  /**
   * Returns all the lines belonging to one log event. The returned lines are always the same in subsequent calls of
   * this method.
   *
   * @return The lines. At least 1.
   */
  def lines: List[Line]

  /**
   * Returns all {@linkplain #lines lines} concatenated as in the original log source. This method uses caching, so
   * it is fast if called several times.
   *
   * @return The content. Not <code>null</code>.
   */
  def content: String = {
    var c = _contentCache
    if (c == null) {
      // May be entered several times if called by multiple threads. This is the compromise for not using locks. 
      c = lines.foldLeft("")((con, line) => con + line.content + line.lineBreak.value)
      _contentCache = c
    }
    c
  }

  def volume =
    lines.foldLeft(0L)((sum, line) => { sum + line.volume })

  /**
   * Returns the {@linkplain #content content}.
   */
  override def toString = content
}