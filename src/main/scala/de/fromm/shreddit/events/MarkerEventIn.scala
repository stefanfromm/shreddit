package de.fromm.shreddit.events

import de.fromm.shreddit.util.RegexUtil
import de.fromm.shreddit.lines.LineIn
import scala.util.matching.Regex
import org.slf4j.LoggerFactory
import de.fromm.shreddit.lines.Line

/**
 * @param startMarker
 * @param maxEventLength
 * @param lineIn
 * 
 * @author Stefan Fromm
 */
class MarkerEventIn(val startMarker: Regex, val maxEventLength: Int, private val lineIn: LineIn) extends EventIn {
  require(startMarker != null)
  require(!startMarker.pattern.pattern.isEmpty)
  require(lineIn != null)
  require(maxEventLength > 0)

  private final val LOG = LoggerFactory.getLogger(classOf[MarkerEventIn])

  private var _previousStartLine = Option.empty[Line]

  def this(startMarker: String, maxEventLength: Int, lineIn: LineIn) =
    this(RegexUtil.oneOccurrenceOf(startMarker), maxEventLength, lineIn)

  def read: Option[Event] = {
    case class Section(val lineCount: Long, val lines: List[Line], startLineAfter: Option[Line])

    def readLinesUntilNextStartLine(maxLength: Long, ignore: Boolean): Section = {
      var lineCount = 0
      var lines = List.empty[Line]
      var startLine = Option.empty[Line]

      if (lineCount < maxLength) {
        var line = lineIn.read
        while (line.isDefined && startLine.isEmpty && lineCount < maxLength) {
          if (startMarker.pattern.matcher(line.get.content).matches) {
            startLine = line
          } else {
            if (!ignore)
              lines = lines ++ List(line.get)
            lineCount += 1
            line = lineIn.read
          }
        }
      }

      Section(lineCount, lines, startLine)
    }

    var startLine = _previousStartLine
    if (startLine.isEmpty) {
      // skip ignored lines, e.g. at the beginning of a file or after the last event having been cut off because of having
      // exceeded max. event length (# lines)
      val ignored = readLinesUntilNextStartLine(Long.MaxValue, true)
      if (ignored.lineCount > 0) {
        LOG.warn("Ignored {} lines.", ignored.lineCount)
      }
      startLine = ignored.startLineAfter
    }

    if (startLine.isEmpty)
      // no next start line found, so there is no next event
      None
    else {
      // read at most maxEventLength lines or until the next start line

      var curEventLength = 1 // start line belongs to event

      val event = readLinesUntilNextStartLine(maxEventLength - curEventLength, false)
      _previousStartLine = event.startLineAfter
      Some(new Event {
        def lines = List(startLine.get) ++ event.lines
      })
    }
  }

  def estimateVolume: Long =
    lineIn.estimateVolume

  def close {
    lineIn.close
  }
}