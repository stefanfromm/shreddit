package de.fromm.shreddit.events

import de.fromm.shreddit.lines.LineIn

/**
 * A source of events creating one event per line in a log file.
 * 
 * @author Stefan Fromm
 */
class LineEventIn(private val lineIn: LineIn) extends EventIn {
  require(lineIn != null)

  def read = {
    lineIn.read match {
      case Some(line) => Some(new Event {
        val lines = List(line)
      })
      case None => None
    }
  }

  def estimateVolume: Long =
    lineIn.estimateVolume

  def close {
    lineIn.close
  }
}

