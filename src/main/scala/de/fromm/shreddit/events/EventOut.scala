package de.fromm.shreddit.events

import de.fromm.shreddit.lines.LineOut

/**
 * A sink of events.
 *
 * @author Stefan Fromm
 */
trait EventOut {
  def write(event: Event)

  def close
}

object EventOut {
  def apply(lineOut: LineOut): EventOut = new EventOut {
    def close {
      lineOut.close
    }

    def write(event: Event) {
      event.lines.foreach(lineOut.write(_))
    }
  }
}

