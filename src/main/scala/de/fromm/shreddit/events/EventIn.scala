package de.fromm.shreddit.events

import de.fromm.shreddit.lines.LineIn
import de.fromm.shreddit.lines.LineBreak
import scala.util.matching.Regex
import de.fromm.shreddit.util.RegexUtil
import de.fromm.shreddit.lines.Line
import org.slf4j.LoggerFactory

/**
 * A source of events.
 *
 * @author Stefan Fromm
 */
trait EventIn {
  def read: Option[Event]

  def estimateVolume: Long

  def close
}
