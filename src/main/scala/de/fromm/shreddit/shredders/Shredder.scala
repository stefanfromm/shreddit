package de.fromm.shreddit.shredders

import de.fromm.shreddit.events.Event
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.io.IoControl

/**
 * docme: Shredder
 *
 * @author Stefan Fromm
 */
trait Shredder {
  def next(event: Event, io: IoControl)

  def start(step: Step, io: IoControl)

  def stop(io: IoControl)
}