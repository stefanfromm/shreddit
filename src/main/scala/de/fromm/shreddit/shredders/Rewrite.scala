package de.fromm.shreddit.shredders

import de.fromm.shreddit.io.IoControl
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.events.Event
import scala.util.matching.Regex
import de.fromm.shreddit.util.RegexUtil
import de.fromm.shreddit.lines.Line
import de.fromm.shreddit.lines.StringIn

/**
 * docme: Rewrite
 *
 * @author Stefan Fromm
 */
class Rewrite(val detectionPattern: Regex, val replacePattern: Regex, val replacement: Option[String]) extends Shredder {
  require(detectionPattern != null)
  require(replacePattern != null)
  require(replacement != null)

  def this(detectionPattern: Option[String], replacePattern: String, replacement: Option[String]) =
    this(
      detectionPattern match {
        case Some(dp) => RegexUtil.oneOccurrenceOf(dp)
        case None => RegexUtil.oneOccurrenceOf(replacePattern)
      },
      RegexUtil.multiOccurrencesOf(replacePattern),
      replacement)

  def next(event: Event, io: IoControl) {
    def splitLines(content: String): List[Line] =
      new StringIn(content).toList

    def matchesAnyLine: Boolean =
      event.lines.exists(line => { detectionPattern.pattern.matcher(line.content).matches })

    if (matchesAnyLine) {
      io.passOn(
        event.lines.flatMap(l => {
          //todo: provide original line break to last line
          splitLines(replacePattern.replaceAllIn(l.content, replacement match {
            case Some(r) => r
            case None => ""
          }))

        }))
    } else
      io.passOn
  }

  def start(step: Step, io: IoControl) {
  }

  def stop(io: IoControl) {
  }
}