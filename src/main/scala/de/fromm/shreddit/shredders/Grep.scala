package de.fromm.shreddit.shredders

import scala.util.matching.Regex
import de.fromm.shreddit.io.Io
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.events.Event
import java.util.Deque
import java.util.ArrayDeque
import de.fromm.shreddit.io.IoControl
import de.fromm.shreddit.util.RegexUtil

/**
 * docme: Grep
 *
 * @author Stefan Fromm
 */
class Grep(val pattern: Regex, val before: Int, val after: Int, val reverse: Boolean) extends Shredder {
  require(pattern != null)
  require(!pattern.pattern.pattern.isEmpty)
  //todo: check, if pattern is empty?
  require(before >= 0)
  require(after >= 0)

  def this(pattern: String, before: Int, after: Int, reverse: Boolean) =
    this(RegexUtil.oneOccurrenceOf(pattern), before, after, reverse)

  private var _eventBuffer: List[Event] = Nil
  private var _currentAfter = 0

  def next(event: Event, io: IoControl) {
    if (reverse)
      exclude(event, io)
    else
      include(event, io)
  }

  private def include(event: Event, io: IoControl) {
    if (matches(event)) {
      flushEvents(io)
      io.passOn
      _currentAfter = 0
    } else if (_currentAfter < after) {
      io.passOn
      _currentAfter += 1
    } else if (before > 0) {
      if (_eventBuffer.size == before)
        _eventBuffer = _eventBuffer.tail
      _eventBuffer = _eventBuffer ++ List(event)
    }
  }

  private def exclude(event: Event, io: IoControl) {
    if (matches(event)) {
      _currentAfter = 0
      _eventBuffer = Nil
    } else if (_currentAfter < after) {
      _currentAfter += 1
    } else if (before > 0) {
      if (_eventBuffer.size == before) {
        io.passOn(_eventBuffer.head)
        _eventBuffer = _eventBuffer.tail
      }
      _eventBuffer = _eventBuffer ++ List(event)
    } else
      io.passOn
  }

  private def flushEvents(io: IoControl) {
    _eventBuffer.foreach(io.passOn(_))
    _eventBuffer = Nil
  }

  private def matches(event: Event) = {
    event.lines.find(line => { pattern.findFirstMatchIn(line.content).isDefined }).isDefined
  }

  def start(step: Step, io: IoControl) {
    _currentAfter = after
  }

  def stop(io: IoControl) {
    if (reverse) {
      flushEvents(io)
    } else
      _eventBuffer = Nil
  }
}