package de.fromm.shreddit.shredders

import de.fromm.shreddit.io.IoControl
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.events.Event
import de.fromm.shreddit.util.RegexUtil
import org.joda.time.DateTime
import org.slf4j.LoggerFactory

/**
 * docme: DateRange
 *
 * @author Stefan Fromm
 */
class DateRange(val dateExtractor: DateExtractor, val from: Option[DateTime], val until: Option[DateTime]) extends Shredder {
  private final val LOG = LoggerFactory.getLogger(classOf[DateRange])

  require(dateExtractor != null)
  require(from != null)
  require(until != null)

  private var _inRange = false
  private var _until = until.getOrElse(new DateTime())

  def next(event: Event, io: IoControl) {
    dateExtractor.extract(event) match {
      case Some(dateTime) => {
        val from = this.from.getOrElse(DateRange.from1970)
        val until = _until
        
        if (!dateTime.isBefore(from) && !dateTime.isAfter(until)) {
          if (!_inRange) {
            LOG.info("Entered date range from '{}'.", dateExtractor.dateFormat.print(from))
            _inRange = true
          }
        } else {
          if (_inRange) {
            LOG.info("Left date range until '{}'.", dateExtractor.dateFormat.print(until))
            _inRange = false
          }
        }
      }
      case None => {
      }
    }

    if (_inRange) {
      io.passOn(event)
    }
  }

  def start(step: Step, io: IoControl) {
    _inRange = false
    _until = until.getOrElse(new DateTime())
  }

  def stop(io: IoControl) {
    _inRange = false
  }
}

/**
 * @author Stefan Fromm
 */
private object DateRange {
  val from1970 = new DateTime(0)
}