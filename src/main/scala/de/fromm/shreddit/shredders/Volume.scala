package de.fromm.shreddit.shredders

import de.fromm.shreddit.io.IoControl
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.events.Event
import org.slf4j.LoggerFactory
import scala.util.matching.Regex
import de.fromm.shreddit.lines.LineBreak
import de.fromm.shreddit.util.RegexUtil

/**
 * docme: Volume
 *
 * @author Stefan Fromm
 */
class Volume(val categoryPatterns: Iterable[Regex]) extends Shredder {
  private final val LOG = LoggerFactory.getLogger(classOf[Volume])

  type Category = String

  private var _volumes = Map.empty[Category, CategoryVolume]

  def this(categoryPattern: List[String]) =
    this(categoryPattern.map(RegexUtil.oneOccurrenceOf(_)))

  def extractCategory(event: Event): Option[String] = {
    categoryPatterns.iterator.flatMap(cp => { event.lines.map(line => cp.pattern.matcher(line.content)) }).find(_.matches) match {
      case Some(m) => Some(if (m.groupCount > 0) m.group(1) else "others")
      case None => None
    }
  }

  def next(event: Event, io: IoControl) {
    extractCategory(event) match {
      case Some(category) => {
        var stats = _volumes.get(category)
        if (stats.isEmpty) {
          stats = Some(new CategoryVolume(category))
          _volumes = _volumes + (category -> stats.get)
        }
        stats.get.occurs(event.lines.foldLeft(0L)((sum, line) => { sum + line.content.length + line.lineBreak.value.length }))
      }
      case None =>
    }

    io.passOn
  }

  def start(step: Step, io: IoControl) {
    _volumes = Map()
  }

  def stop(io: IoControl) {
    def toString(mv: CategoryVolume): String = {
      "%s: %d chars in %d log events".format(mv.category, mv.volume, mv.occurrences)
    }

    val markerVolumes = _volumes.values.toList.sortBy(_.volume).reverse
    LOG.info(markerVolumes.map(toString(_)).mkString("Volumes:" + LineBreak.Auto.value + "  ", LineBreak.Auto.value + "  ", ""))
  }

  private class CategoryVolume(val category: Category) {
    require(category != null)
    require(!category.isEmpty)

    private var _occurrences = 0L
    private var _volume = 0L

    def occurs(volume: Long) {
      _occurrences += 1
      _volume += volume
    }

    def occurrences =
      _occurrences

    def volume =
      _volume
  }
}