package de.fromm.shreddit.shredders

import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.io.IoControl
import de.fromm.shreddit.events.Event

/**
 * docme: Progress
 *
 * @author Stefan Fromm
 */
class Progress(private val estimatedVolume: Long, private val listener: Progress.Listener) extends Shredder {
  private var _currentVolume: Long = 0
  private var _lastPercent: Int = 0

  def next(event: Event, io: IoControl) {
    _currentVolume += event.volume
    val currentPercent = Math.min(_currentVolume.toDouble / estimatedVolume * 100, 100).toInt
    listener.absoluteProgress(_currentVolume)
    if (currentPercent != _lastPercent) {
      _lastPercent = currentPercent
      listener.relativeProgress(_lastPercent)
    }
    io.passOn
  }

  def start(step: Step, io: IoControl) {
    _currentVolume = 0L
    _lastPercent = 0
    listener.absoluteProgress(0)
    listener.relativeProgress(0)
  }

  def stop(io: IoControl) {
    if (_lastPercent < 100) {
      listener.absoluteProgress(estimatedVolume)
      listener.relativeProgress(100)
    }
    _currentVolume = 0L
    _lastPercent = 0
  }
}

/**
 * @author Stefan Fromm
 */
object Progress {
  /**
   * @author Stefan Fromm
   */
  trait Listener {
    def absoluteProgress(currentVolume: Long)

    def relativeProgress(currentPercent: Int)
  }
}