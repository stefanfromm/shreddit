package de.fromm.shreddit.shredders

import scala.util.matching.Regex
import org.joda.time.format.DateTimeFormatter
import de.fromm.shreddit.events.Event
import org.joda.time.DateTime
import de.fromm.shreddit.util.RegexUtil
import org.joda.time.format.DateTimeFormat

/**
 * docme: DateExtractor
 *
 * @author Stefan Fromm
 */
class DateExtractor(val dateDetectionPattern: Regex, val dateFormat: DateTimeFormatter) {
  require(dateDetectionPattern != null)
  require(!dateDetectionPattern.pattern.pattern.isEmpty)
  //todo: check that a group is defined
  require(dateFormat != null)

  def this(dateDetectionPattern: String, dateFormat: String) =
    this(RegexUtil.oneOccurrenceOf(dateDetectionPattern), DateTimeFormat.forPattern(dateFormat))

  def extract(event: Event): Option[DateTime] = {
    val matcher = dateDetectionPattern.pattern.matcher(event.content)
    if (matcher.matches && matcher.groupCount > 0)
      Some(dateFormat.parseDateTime(matcher.group(1)))
    else
      None
  }
}

/**
 * @author Stefan Fromm
 */
object DateExtractor {
  //todo: default dates
}