package de.fromm.shreddit.shredders

import de.fromm.shreddit.events.Event
import org.slf4j.LoggerFactory
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.io.IoControl

/**
 * docme: LineCounter
 *
 * @author Stefan Fromm
 */
class LineCounter extends Shredder {
  private val _log = LoggerFactory.getLogger(classOf[LineCounter])

  private var _lineCount = 0L
  private var _step: Step = null

  def next(event: Event, io: IoControl) {
    _lineCount += event.lines.size
    //todo: log line count every 100.000 lines
    io.passOn
  }

  def start(step: Step, io: IoControl) {
    //todo: prevent start, if started already thus preventing being used in two steps
    _step = step
    _lineCount = 0L
  }

  def stop(io: IoControl) {
    //todo: log line count only, if not dividable by 100.000
    _log.info("Processed {} lines in step {}", _lineCount, _step.name)
    _step = null
  }

  def lineCount =
    _lineCount
}
