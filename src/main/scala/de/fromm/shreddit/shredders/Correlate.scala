package de.fromm.shreddit.shredders

import de.fromm.shreddit.io.IoControl
import de.fromm.shreddit.pipe.Step
import de.fromm.shreddit.events.Event
import scala.util.matching.Regex
import java.util.regex.Matcher
import de.fromm.shreddit.util.RegexUtil

/**
 * docme: Correlate
 *
 * @author Stefan Fromm
 */
class Correlate(
    val startPattern: Regex,
    val correlationPattern: String,
    val cancelPattern: Option[String],
    val dateExtractor: Option[DateExtractor],
    val maxTimeLimit: Option[Long],
    val warnNonCorrelated: Boolean,
    val printStatistics: Boolean) extends Shredder {

  require(startPattern != null)
  require(!startPattern.pattern.pattern.isEmpty)

  def this(startPattern: String, correlationPattern: String, cancelPattern: Option[String], dateExtractor: Option[DateExtractor], maxTimeLimit: Option[Long], warnNonCorrelated: Boolean, printStatistics: Boolean) =
    this(RegexUtil.oneOccurrenceOf(startPattern), correlationPattern, cancelPattern, dateExtractor, maxTimeLimit, warnNonCorrelated, printStatistics)

  private var _openCorrelations = List.empty[OpenCorrelation]
  private var _matchedCorrelations = 0
  private var _unmatchedCorrelations = 0
  private var _maxOpenCorrelations = 0

  def next(event: Event, io: IoControl) {
    var oc = _openCorrelations.reverse.find(_.isCanceledBy(event))
    if (oc.isEmpty) {
      oc = _openCorrelations.find(_.correlatesTo(event))
    }

    oc match {
      case Some(c) => {
        _openCorrelations = _openCorrelations diff List(c) // remove
        _matchedCorrelations += 1
        _unmatchedCorrelations -= 1

        io.passOn(c.startEvent)
        io.passOn(event)
        
        dateExtractor map { ex =>
          val startDate = ex.extract(c.startEvent)
          val endDate = ex.extract(event)
          if (startDate.isDefined && endDate.isDefined) {
            val timeDiff = endDate.get.getMillis - startDate.get.getMillis
            var timeWarning = timeDiff + " ms"
            maxTimeLimit.map { limit =>
              if (timeDiff > limit)
                timeWarning += " !!! longer than " + limit + " ms"
            }
            io.passOn(timeWarning)
          }
        }

      }
      case None => {
        val startMatcher = startPattern.pattern.matcher(event.content)
        if (startMatcher.matches) {
          _openCorrelations = _openCorrelations ++ List(new OpenCorrelation(event, startMatcher, correlationPattern, cancelPattern))
          _maxOpenCorrelations = Math.max(_maxOpenCorrelations, _openCorrelations.size)
          _unmatchedCorrelations += 1
        }
      }
    }
  }

  private def reset() {
    _openCorrelations = List.empty[OpenCorrelation]
    _matchedCorrelations = 0
    _unmatchedCorrelations = 0
    _maxOpenCorrelations = 0
  }

  def start(step: Step, io: IoControl) {
    reset()
  }

  def stop(io: IoControl) {
    if (!_openCorrelations.isEmpty && warnNonCorrelated) {
      io.passOn("")
      io.passOn("!!! Found start entries without correlated ending or canceling entries !!!")
      io.passOn("")
      _openCorrelations.foreach(oc => io.passOn(oc.startEvent))
    }
    if (printStatistics) {
      io.passOn("")
      io.passOn("Statistics")
      io.passOn("")
      io.passOn("  max. # of open correlations at the same time: %d".format(_maxOpenCorrelations))
      io.passOn("  # of matched correlations                   : %d".format(_matchedCorrelations))
      io.passOn("  # of unmatched correlations                 : %d".format(_unmatchedCorrelations))
    }
    reset()
  }

  private final class OpenCorrelation(val startEvent: Event, private val startMatch: Matcher, ep: String, cp: Option[String]) {
    private val endPattern: Regex = createReferencePattern(startMatch, ep)
    private val cancelPattern: Option[Regex] = cp match {
      case Some(p) => Some(createReferencePattern(startMatch, p))
      case None => None
    }

    def correlatesTo(event: Event): Boolean =
      endPattern.pattern.matcher(event.content).matches

    def isCanceledBy(event: Event): Boolean = cancelPattern match {
      case Some(cp) => cp.pattern.matcher(event.content).matches
      case None => false
    }

    private def createReferencePattern(reference: Matcher, pattern: String): Regex = {
      // construct end pattern by replacing $i references inside it with the group found in the start pattern. The
      // group gets quoted because the group's content could contain control characters for regular expressions.

      val groupRefMatcher = RegexUtil.GROUP_REF.pattern.matcher(pattern)
      val sb = new StringBuffer
      while (groupRefMatcher.find()) {
        val index = groupRefMatcher.group(1).toInt
        if (index <= reference.groupCount) {
          groupRefMatcher.appendReplacement(
            sb,
            java.util.regex.Pattern.quote(reference.group(index)).replace("""\""", """\\"""))
        } else {
          groupRefMatcher.appendReplacement(sb, groupRefMatcher.group)
        }
      }
      groupRefMatcher.appendTail(sb)
      RegexUtil.oneOccurrenceOf(sb.toString)
    }

    override def toString = startMatch.group + ".." + endPattern.pattern.pattern + (cancelPattern match {
      case Some(cp) => " canceled by " + cp.pattern.pattern
      case None => ""
    })
  }
}