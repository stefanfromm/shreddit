package de.fromm.shreddit.util

/**
 * docme: Enum
 *
 *
 * @author Stefan Fromm
 */
trait Enum[A] {
  // source 1: http://stackoverflow.com/questions/1898932/case-objects-vs-enumerations-in-scala
  // source 2: http://stackoverflow.com/questions/14947179/using-a-custom-enum-in-the-scala-worksheet-i-am-receiving-an-error-java-lang-ex

  //fixme: subtle bugs see http://stackoverflow.com/a/8620085
  trait Value { self: A =>
    _values :+= this
  }

  private var _values = List.empty[A]

  def values = _values
}