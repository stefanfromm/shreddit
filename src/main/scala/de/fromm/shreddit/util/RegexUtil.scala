package de.fromm.shreddit.util

import scala.util.matching.Regex

/**
 * docme: RegexUtil
 *
 * @author Stefan Fromm
 */
object RegexUtil {
  val GROUP_REF = """(?<!\\)\$([0-9])""".r

  /**
   * docme: oneOccurrenceOf()
   *
   * @param pattern
   * @return
   */
  def oneOccurrenceOf(pattern: String): Regex = {
    var compiledPattern = "(?:" + pattern + ")" // dot all mode
    if (!pattern.startsWith("^"))
      compiledPattern = "^.*" + compiledPattern

    if (!pattern.endsWith("$") || pattern.endsWith("\\$")) //fixme: count occurrences of \ before $
      compiledPattern += ".*$"

    ("(?s)" + compiledPattern).r
  }

  /**
   * docme: multiOccurrencesOf()
   *
   * @param pattern
   * @return
   */
  def multiOccurrencesOf(pattern: String): Regex = {
    ("(?s)" + pattern).r
  }
}