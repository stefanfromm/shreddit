package de.fromm.shreddit.cli

import java.io.File

/**
 * docme: StorageService
 *
 * @author Stefan Fromm
 */
trait StorageServiceComponent {
  val storageService: StorageService

  /**
   * @author Stefan Fromm
   */
  trait StorageService {
    def loadPipes(source: Option[File] = None): CliPipes
    def savePipes(pipes: CliPipes, target: Option[File] = None)
  }
}