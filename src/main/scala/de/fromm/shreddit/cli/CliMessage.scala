package de.fromm.shreddit.cli

import de.fromm.shreddit.util.Enum

/**
 * docme: CliMessage
 *
 * @author Stefan Fromm
 */
sealed case class CliMessage(val content: String, val level: CliMessageLevel) {
  def this(content: String) = this(content, CliMessageLevel.Info)
}

object CliMessage {
  implicit def apply(message: String) = new CliMessage(message)

  def info(message: String) = new CliMessage(message)
  def warn(message: String) = new CliMessage(message, CliMessageLevel.Warn)
  def error(message: String) = new CliMessage(message, CliMessageLevel.Error)
}

sealed trait CliMessageLevel extends CliMessageLevel.Value

object CliMessageLevel extends Enum[CliMessageLevel] {
  case object Info extends CliMessageLevel
  case object Warn extends CliMessageLevel
  case object Error extends CliMessageLevel
}