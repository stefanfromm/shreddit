package de.fromm.shreddit.cli

/**
 * docme: CliPipes
 *
 * @author Stefan Fromm
 */
class CliPipes {
  private var _constants = Map.empty[String, String]
  private var _pipes = List.empty[CliPipe]
  private var _currentPipe: Option[CliPipe] = None

  def setConstant(name: String, value: String): Option[String] = {
    val oldValue = _constants.get(name)
    _constants = _constants + (name -> value)
    oldValue
  }

  def removeConstant(name: String): Option[String] = {
    val oldValue = _constants.get(name)
    _constants = _constants - name
    oldValue
  }

  def constants =
    _constants

  def addPipe(name: String): Option[CliPipe] = {
    _pipes.find(_.name == name) match {
      case Some(_) => None
      case None => {
        val p = new CliPipe(name)
        _pipes = _pipes ++ List(p)
        currentPipe = name

        Some(p)
      }
    }
  }

  def removePipe(name: String): Option[CliPipe] =
    pipeByName(name) match {
      case Some(pipe) => {
        _pipes = _pipes diff List(pipe)
        if (_currentPipe.isDefined && pipe == _currentPipe.get)
          _currentPipe = None
        Some(pipe)
      }
      case None => None
    }

  def pipeByName(name: String): Option[CliPipe] =
    _pipes.find(_.name == name)

  def pipes =
    _pipes

  def clear {
    _pipes = Nil
    _constants = Map.empty[String, String] //todo: define default constants
  }

  def currentPipe =
    _currentPipe

  def currentPipe_=(name: String): Option[CliPipe] = {
    _currentPipe = pipeByName(name)
    _currentPipe
  }
}