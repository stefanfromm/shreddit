package de.fromm.shreddit.cli

import de.fromm.shreddit.shredders.Shredder

/**
 * docme: CliShredder
 *
 * @author Stefan Fromm
 */
abstract class CliShredder(val id: String) {
  def toShredder(options: PipeOptions): Shredder
}