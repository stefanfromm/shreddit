package de.fromm.shreddit.cli

/**
 * docme: RemoveStepFromCurrentPipe
 *
 * @author Stefan Fromm
 */
case class RemoveStepFromCurrentPipe(val stepName: String) extends CliCommand {
  private var success = false
  private var removed: CliStep = null

  def exec(pipes: CliPipes, cli: CliCallback) {
    success = false // in case of several calls to this command
    pipes.currentPipe match {
      case Some(currentPipe) => {
        val removeResult = currentPipe.removeStep(stepName)
        if (removeResult.isDefined) {
          removed = removeResult.get
          success = true
          cli.showMessages("Step \"%s\" removed from pipe \"%s\".".format(stepName, currentPipe.name))
        } else
          cli.showMessages(CliMessage.error("Step \"%s\" did not exist in pipe \"%s\" anymore.".format(stepName, currentPipe.name)))
      }
      case None =>
        cli.showMessages(CliMessage.error("Please select a current pipe first!"))
    }
  }

  override def back = if (success) Some(AddStepToCurrentPipe(stepName, removed.shredder)) else None
}