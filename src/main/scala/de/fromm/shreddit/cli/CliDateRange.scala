package de.fromm.shreddit.cli

import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.shredders.DateRange
import de.fromm.shreddit.shredders.DateExtractor
import org.joda.time.format.DateTimeFormat

/**
 * @author Stefan Fromm
 */
class CliDateRange extends CliShredder("daterange") {
  var dateExtractionPattern: String = null
  var dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS"
  var from: Option[String] = None
  var until: Option[String] = None
  
  def toShredder(options: PipeOptions): Shredder = {
    val dateTimeFormatter = DateTimeFormat.forPattern(options.eval(dateFormat))
    new DateRange(
      new DateExtractor(options.eval(dateExtractionPattern), options.eval(dateFormat)),
      from.map(f => dateTimeFormatter.parseDateTime(options.eval(f))),
      until.map(u => dateTimeFormatter.parseDateTime(options.eval(u)))
    )
  }
}