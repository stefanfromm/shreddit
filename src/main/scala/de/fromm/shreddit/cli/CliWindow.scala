package de.fromm.shreddit.cli

import com.googlecode.lanterna.gui.Window
import com.googlecode.lanterna.gui.Border
import com.googlecode.lanterna.gui.component.TextBox
import com.googlecode.lanterna.terminal.TerminalSize
import com.googlecode.lanterna.input.Key
import com.googlecode.lanterna.gui.Interactable
import com.googlecode.lanterna.gui.listener.WindowAdapter
import com.googlecode.lanterna.gui.component.Panel
import com.googlecode.lanterna.gui.component.Label
import com.googlecode.lanterna.gui.component.TextArea
import com.googlecode.lanterna.gui.Interactable.FocusChangeDirection
import java.util.EnumSet
import com.googlecode.lanterna.gui.component.Separator
import org.slf4j.LoggerFactory
import org.slf4j.Logger
import ch.qos.logback.core.Appender
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.DateTimeFormat
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.filter.ThresholdFilter
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import scala.collection.JavaConversions
import de.fromm.shreddit.lines.LineBreak
import java.io.File
import com.googlecode.lanterna.gui.GUIScreen
import com.googlecode.lanterna.screen.Screen

/**
 * docme: CliWindow
 *
 * @author Stefan Fromm
 */
class CliWindow extends Window("") with CliParser {
  //todo: manage a separate command line history for offering arrow up/down in input field; it will be loaded on start, while the command history remains empty
  //fixme: running a pipe twice does not work
  //fixme: re-running a command from the history cuts off all commands after it, should be appended instead, if it is not the last???
  //review: StepChain.next
  //todo: progress bar

  private final val LOG = LoggerFactory.getLogger(classOf[CliWindow])
  private final val LOG_AREA_LINES = 5

  private var commandHistory = List.empty[CmdExecution] // history of all commands with their command line
  private var commandFuture = List.empty[CmdExecution] // future of all commands with their command line
  private val cliPipes = new CliPipes
  private val cliCallback = new CliCallback {
    def setPipe(name: Option[String]) {
      cmdPrompt.setText(name match { case Some(n) => "%s>".format(n) case None => ">" })
    }
    def quit {
      logAreaAppender.stop
      rootLogger.detachAppender(logAreaAppender)
      CliWindow.this.getOwner.getScreen.stopScreen
      System.exit(0)
    }
    def showMessages(messages: CliMessage*) {
      messageArea.clear
      appendMessages0(messages)
    }
    def showMessages(messages: List[CliMessage]) {
      messageArea.clear
      appendMessages0(messages)
    }
    def appendMessages(messages: CliMessage*) {
      appendMessages0(messages)
    }
    def appendMessages(messages: List[CliMessage]) {
      appendMessages0(messages)
    }

    private def appendMessages0(messages: Seq[CliMessage]) {
      //todo: give different colors to different message levels
      messages.foreach(m => messageArea.appendLine(m.content))
    }

    def commandHistory: List[CliCommand] =
      CliWindow.this.commandHistory.filter(_.cmd.isDefined).map(_.cmd.get)

    def commandFuture: List[CliCommand] =
      CliWindow.this.commandFuture.filter(_.cmd.isDefined).map(_.cmd.get)

    def bringLogToFront {
      logPanel.setVisible(true)
      resizeControls
      setFocus(logArea)
    }
  }

  private val cmdPrompt = new Label(">")

  private val cmdLine = new TextBox with CliCopyable with CliPastable {
    override def keyboardInteraction(key: Key) = {
      var result = super.keyboardInteraction(key)
      try {
        result =
          if (key.getKind == Key.Kind.NormalKey && key.getCharacter == 'z' && key.isCtrlPressed && !key.isAltPressed) {
            revokeLastCommand()
            setText("")
            Interactable.Result.EVENT_HANDLED
          } else if (key.getKind == Key.Kind.NormalKey && key.getCharacter == 'y' && key.isCtrlPressed && !key.isAltPressed) {
            reexecuteLastRevokedCommand()
            setText("")
            Interactable.Result.EVENT_HANDLED
          } else if (key.getKind == Key.Kind.ArrowUp && !key.isAltPressed && !key.isCtrlPressed) {
            setText(returnToPreviousCmdLine())
            Interactable.Result.EVENT_HANDLED
          } else if (key.getKind == Key.Kind.ArrowDown && !key.isAltPressed && !key.isCtrlPressed) {
            setText(forwardToNextCmdLine())
            Interactable.Result.EVENT_HANDLED
          } else if (key.getKind == Key.Kind.Enter && !key.isAltPressed && !key.isCtrlPressed) {
            executeCommand(getText)
            setText("")
            Interactable.Result.EVENT_HANDLED
          } else
            result
      } catch {
        case e: Throwable =>
          e.printStackTrace
          setText("")
          cliCallback.showMessages(CliMessage.error(e.getMessage))
          result = Interactable.Result.EVENT_HANDLED
      }
      result
    }
  }
  cmdLine.setFillCharacter('_')

  private val cmdInput = new Panel(Panel.Orientation.HORISONTAL)
  cmdInput.addComponent(cmdPrompt)
  cmdInput.addComponent(cmdLine)
  addComponent(cmdInput)

  private val messageArea: TextArea = new ScrollableTextArea
  addComponent(messageArea)

  //todo: load current log file from logging system
  private val logSeparator = new Label("Latest Log (for full log see %s%slogs%sshreddit.log)".format(System.getProperty("user.dir"), File.separator, File.separator))
  private val logPanel = new Panel(Panel.Orientation.VERTICAL)
  private val logArea = new ScrollableTextArea {
    override protected def afterLeftFocus(direction: FocusChangeDirection) {
      logPanel.setVisible(false)
      resizeControls
    }
  }
  logPanel.addComponent(logSeparator)
  logPanel.addComponent(logArea)
  logPanel.setVisible(false)

  addComponent(logPanel);

  // init window
  setBorder(new Border.Invisible)
  setDrawShadow(false)
  addWindowListener(new WindowAdapter {
    override def onWindowShown(window: Window) {
      resizeControls
      exec(Welcome())
    }
  })
  cliCallback.setPipe(None)

  private def resizeControls {
    val screenSize = getOwner.getScreen.getTerminalSize
    val screenCols = screenSize.getColumns
    val screenRows = screenSize.getRows
    println("screen size: " + screenSize)
    cmdInput.setPreferredSize(new TerminalSize(screenCols, cmdInput.getPreferredSize.getRows))
    cmdLine.setPreferredSize(new TerminalSize(screenCols - cmdPrompt.getText.length, cmdLine.getPreferredSize.getRows))
    logSeparator.setPreferredSize(new TerminalSize(screenCols, logSeparator.getPreferredSize.getRows))
    logArea.setPreferredSize(new TerminalSize(screenCols, logArea.getPreferredSize.getRows))
    if (logPanel.isVisible) {
      logPanel.setPreferredSize(new TerminalSize(screenCols, Math.min(LOG_AREA_LINES, 10) + logSeparator.getPreferredSize.getRows))
      logArea.setPreferredSize(new TerminalSize(screenCols, Math.min(LOG_AREA_LINES, 10)))
      messageArea.setPreferredSize(new TerminalSize(screenCols, screenRows - cmdInput.getPreferredSize.getRows - logPanel.getPreferredSize.getRows))
    } else {
      logPanel.setPreferredSize(new TerminalSize(screenCols, 0))
      logArea.setPreferredSize(new TerminalSize(screenCols, 0))
      messageArea.setPreferredSize(new TerminalSize(screenCols, screenRows - cmdInput.getPreferredSize.getRows))
    }
  }

  private final val logAreaAppender: Appender[ILoggingEvent] = new AppenderBase[ILoggingEvent] {
    private final val DATE_TIME_FORMAT = DateTimeFormat.forStyle("MM")
    private final val MAX_LINES = 100

    private var _lineCount = 0

    override protected def append(event: ILoggingEvent) {
      try {
        val newLines = (DATE_TIME_FORMAT.print(event.getTimeStamp()) + " - " + event.getFormattedMessage).split("""[\n\r]+""").takeRight(MAX_LINES)

        while (_lineCount > MAX_LINES - newLines.length) {
          logArea.removeLine(_lineCount - 1)
          _lineCount -= 1
        }

        for (line <- newLines) {
          logArea.insertLine(0, line)
          _lineCount += 1
        }

        cliCallback.bringLogToFront
      } catch {
        case e: Exception => {
          e.printStackTrace // todo: log via the logging system (loglog)
        }
      }
    }
  }

  private final val rootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).asInstanceOf[ch.qos.logback.classic.Logger]
  logAreaAppender.setContext(LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext])
  val atLeastInfo = new ThresholdFilter
  atLeastInfo.setLevel("info")
  logAreaAppender.addFilter(atLeastInfo)
  atLeastInfo.start
  logAreaAppender.start
  rootLogger.addAppender(logAreaAppender)

  private def executeCommand(cmdLine: String) {
    cliCallback.showMessages()

    if (cmdLine != "")
      parseAll(command, cmdLine) match {
        case Success(cmd, _) => {
          exec(cmd) //todo: handle error
          commandHistory = commandHistory ++ List(CmdExecution(cmdLine, Some(cmd))) //todo: restrict history length
          commandFuture = Nil
        }
        case NoSuccess(msg, _) => {
          commandHistory = commandHistory ++ List(CmdExecution(cmdLine, None)) //todo: restrict history length
          commandFuture = Nil
          exec(Help(msg)) //todo: if command was parseable show commands help page !!! command name aliases !!!
        }
      }
  }

  private def exec(cmd: CliCommand) {
    val t0 = System.nanoTime
    cmd.exec(cliPipes, cliCallback)
    val t1 = System.nanoTime
    LOG.debug("Executed {} in {} seconds.", cmd.getClass.getSimpleName, (t1 - t0).toDouble / 1E9)
  }

  private def returnToPreviousCmdLine(): String = {
    if (commandHistory.isEmpty)
      ""
    else {
      val cmdExec = commandHistory.last
      commandHistory = commandHistory.dropRight(1)
      commandFuture = List(cmdExec) ++ commandFuture
      cmdExec.cmdLine
    }
  }

  private def forwardToNextCmdLine(): String = {
    if (commandFuture.isEmpty)
      ""
    else {
      val cmdExec = commandFuture.head
      commandHistory = commandHistory ++ List(cmdExec)
      commandFuture = commandFuture.tail
      cmdExec.cmdLine
    }
  }

  private def revokeLastCommand() {
    // go back until next command which is backable, after unbackable commands have been added
    val lastIdx = commandHistory.lastIndexWhere(cmdExec => cmdExec.cmd.isDefined && cmdExec.cmd.get.back.isDefined)
    //todo: reset commands for not re-executing them
    if (lastIdx > -1) {
      val cmdExec = commandHistory(lastIdx)
      exec(cmdExec.cmd.get.back.get) //todo: handle error
      commandFuture = commandHistory.slice(lastIdx, commandHistory.length) ++ commandFuture
      commandHistory = commandHistory.dropRight(commandHistory.length - lastIdx)
    } else
      cliCallback.showMessages("Begin of command history reached. No command revoked.")
  }

  private def reexecuteLastRevokedCommand() {
    // go forth until next command which is backable, after unbackable commands have been added
    val firstIdx = commandFuture.indexWhere(cmdExec => cmdExec.cmd.isDefined)
    if (firstIdx > -1) {
      val cmdExec = commandFuture(firstIdx)
      exec(cmdExec.cmd.get) //todo: handle error
      commandHistory = commandHistory ++ commandFuture.slice(0, firstIdx + 1)
      commandFuture = commandFuture.drop(firstIdx + 1)
    } else
      cliCallback.showMessages("End of command history reached. No command re-executed.")
  }

  private class ScrollableTextArea extends TextArea with CliCopyable {
    private final val _scrollKeys = EnumSet.of(Key.Kind.ArrowDown, Key.Kind.ArrowLeft, Key.Kind.ArrowRight, Key.Kind.ArrowUp, Key.Kind.End, Key.Kind.Home, Key.Kind.PageDown, Key.Kind.PageUp)

    def getText: String = {
      // it is a hack to get the text out of the text area
      val sb = new StringBuilder
      var lineIdx = 0
      var reachedLast = false
      while (!reachedLast) {
        try {
          sb ++= getLine(lineIdx) ++= LineBreak.Auto.value
          lineIdx += 1
        } catch {
          case e: java.lang.ArrayIndexOutOfBoundsException => reachedLast = true
        }
      }
      sb.mkString
    }

    override def keyboardInteraction(key: Key) = {
      var result = super.keyboardInteraction(key)

      if (!_scrollKeys.contains(key.getKind)) {
        setFocus(cmdLine)
        result = Interactable.Result.EVENT_HANDLED
        cmdLine.keyboardInteraction(key)
      }
      result
    }
  }

  private case class CmdExecution(val cmdLine: String, cmd: Option[CliCommand]) {
    require(cmdLine != null)
    require(!cmdLine.isEmpty)
    require(cmd != null)
  }
}
