package de.fromm.shreddit.cli

import scala.util.parsing.combinator.JavaTokenParsers
import scala.util.parsing.combinator.RegexParsers
import scala.util.matching.Regex
import java.io.File
import de.fromm.shreddit.io.TextFile
import java.awt.TextField
import java.nio.charset.Charset
import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.MarkerEventBoundaries
import de.fromm.shreddit.shredders.Volume
import java.util.regex.Pattern

/**
 * docme: CliParser
 *
 * @author Stefan Fromm
 */
trait CliParser extends JavaTokenParsers {
  //todo: add failures "unknown command", "invalid option"
  //todo: run single step on the fly
  //todo: copy pipe
  //todo: init default constants
  //todo: show step with details
  //todo: move steps

  def command: Parser[CliCommand] = quit | savePipes | loadPipes | addPipe | removePipe | showPipes | showPipe | usePipe | runPipe | addStep | removeStep | showConstants | setConstant | unsetConstant | setEventBoundaries | showHistory | showLog | showEnvironment | help | about | listFiles | setWorkingDir | testRegex | failure("No valid command.")

  private def quit: Parser[Quit] = ("quit" | "exit" | "close" | "end" | "bye") ^^
    { _ => Quit() }

  private def help: Parser[Help] = ("help" | "?") ~ opt(commandName) ^^
    {
      case _ ~ commandName => Help(None, commandName)
      case _ => Help()
    }

  private def about: Parser[About] = "about" ^^
    { _ => About() }

  private def commandName: Parser[String] = """\w+""".r

  private def showHistory: Parser[ShowHistory] = "hist" ^^
    { _ => ShowHistory() }

  private def showLog: Parser[ShowLog] = "log" ^^
    { case _ => ShowLog() }

  private def showEnvironment: Parser[ShowEnvironment] = "env" ^^
    { case _ => ShowEnvironment() }

  private def listFiles: Parser[ListFiles] = ("ls" | "dir") ^^
    { case _ => ListFiles() }

  private def setWorkingDir: Parser[SetWorkingDirectory] = "cd" ~ (arg | failure("No working directory.")) ^^
    { case _ ~ dirName => SetWorkingDirectory(dirName) }

  private def testRegex: Parser[TestRegex] = "regex" ~ arg ~ arg ^^
    { case _ ~ regex ~ text => TestRegex(regex, text) }

  // constants

  private def setConstant: Parser[SetConstant] = "set" ~ constName ~ "=" ~ constValue ^^
    { case _ ~ name ~ _ ~ value => SetConstant(name, value) }

  private def unsetConstant: Parser[UnsetConstant] = "unset" ~ constName ^^
    { case _ ~ name => UnsetConstant(name) }

  private def showConstants: Parser[ShowConstants] = "const" ^^
    { _ => ShowConstants() }

  private def constName: Parser[String] = """[^${}=]+""".r
  private def constValue: Parser[String] = """.+""".r

  // common pipe definition

  private def savePipes: Parser[SavePipes] = "save" ~ opt(file) ^^
    {
      case _ ~ file => SavePipes(file)
    }

  private def loadPipes: Parser[LoadPipes] = "load" ~ opt(file) ^^
    {
      case _ ~ file => LoadPipes(file)
    }

  private def file: Parser[File] = arg ^^
    { new File(_) }

  private def textFileExcept(except: String): Parser[TextFile] =
    textFile(Some(except))

  private def textFile(except: Option[String] = None): Parser[TextFile] = arg(except) ~ opt("with" ~ encoding) ^^
    {
      case fileName ~ None => new TextFile(fileName, Charset.defaultCharset)
      case fileName ~ Some(_ ~ encoding) => new TextFile(fileName, encoding)
    }

  private def encoding: Parser[Charset] = """[a-zA-Z0-9\-+.:_]""".r ^^
    { Charset.forName(_) }

  // pipes

  private def addPipe: Parser[AddPipe] = ("addpipe" | "ap" | "newpipe" | "np") ~ pipeName ^^
    { case _ ~ pipeName => AddPipe(pipeName) }

  private def removePipe: Parser[RemovePipe] = ("removepipe" | "rp" | "deletepipe" | "dp") ~ pipeName ^^
    { case _ ~ pipeName => RemovePipe(pipeName) }

  private def usePipe: Parser[UsePipe] = "use" ~ pipeName ^^
    { case _ ~ pipeName => UsePipe(pipeName) }

  //todo: allow to specify sysin and sysout
  private def runPipe: Parser[RunPipe] = "run" ~ rep1(textFileExcept("->")) ~ runOut ^^
    { case _ ~ in ~ out => RunPipe(None, in, out) }

  private def runOut: Parser[Option[TextFile]] = opt("->" ~ textFile()) ^^
    {
      case Some(_ ~ textFile) => Some(textFile)
      case None => None
    }

  private def showPipes: Parser[ShowPipes] = "pipes" ^^
    { _ => ShowPipes() }

  private def showPipe: Parser[ShowPipe] = "pipe" ~ opt(pipeName) ^^
    { case _ ~ n => ShowPipe(n) }

  private def pipeName: Parser[String] = arg

  private def setEventBoundaries: Parser[SetEventBoundariesInCurrentPipe] = "events" ~ (eventPerLine | eventPerMarker) ^^ {
    case _ ~ boundaries => SetEventBoundariesInCurrentPipe(boundaries)
  }

  private def eventPerLine: Parser[OneToOneEventBoundaries] = "line" ^^ {
    case _ => OneToOneEventBoundaries()
  }

  private def eventPerMarker: Parser[MarkerEventBoundaries] = "marker" ~ arg ~ opt("max" ~ int) ^^ {
    case _ ~ startPattern ~ Some(_ ~ maxLength) => MarkerEventBoundaries(startPattern, maxLength)
    case _ ~ startPattern ~ None => MarkerEventBoundaries(startPattern, 16384)
  }

  private def addStep: Parser[AddStepToCurrentPipe] = ("addstep" | "as" | "newstep" | "ns") ~ opt(stepName) ~ shredder ^^
    {
      case _ ~ None ~ shredder => AddStepToCurrentPipe(shredder.id, shredder) // todo: count up counter for step name
      case _ ~ Some(stepName) ~ shredder => AddStepToCurrentPipe(stepName, shredder)
    }

  private def removeStep: Parser[RemoveStepFromCurrentPipe] = ("removestep" | "rs" | "deletestep" | "ds") ~ stepName ^^
    { case _ ~ stepName => RemoveStepFromCurrentPipe(stepName) }

  private def stepName: Parser[String] = arg

  private def shredder: Parser[CliShredder] = grep | rewrite | volume | correlate | daterange

  // grep

  private def grep: Parser[CliGrep] = "grep" ~ arg ~ grepBefore ~ grepAfter ~ grepReverse ^^
    {
      case _ ~ pattern ~ before ~ after ~ reverse =>
        val grep = new CliGrep
        grep.pattern = pattern.r.toString // compile pattern
        before map { grep.before = _ }
        after map { grep.after = _ }
        grep.reverse = reverse
        grep
    }

  private def grepBefore: Parser[Option[Int]] = opt("-" ~ int) ^^
    {
      case Some(_ ~ number) => Some(number)
      case None => None
    }

  private def grepAfter: Parser[Option[Int]] = opt("+" ~ int) ^^
    {
      case Some(_ ~ number) => Some(number)
      case None => None
    }

  private def grepReverse: Parser[Boolean] = opt("reverse") ^^
    {
      case Some(_) => true
      case None => false
    }

  // rewrite

  private def rewrite: Parser[CliRewrite] = "rewrite" ~ arg ~ rewriteOnlyIn ~ rewriteTo ^^
    {
      case _ ~ replacePattern ~ detectionPattern ~ replacement => {
        val rewrite = new CliRewrite
        rewrite.detectionPattern = detectionPattern
        rewrite.replacePattern = replacePattern
        rewrite.replacement = replacement
        rewrite
      }
    }

  private def rewriteOnlyIn: Parser[Option[String]] = opt("if" ~ arg) ^^
    {
      case Some(_ ~ detectionPattern) => Some(detectionPattern.r.toString)
      case None => None
    }

  private def rewriteTo: Parser[Option[String]] = opt("->" ~ arg) ^^
    {
      case Some(_ ~ replacement) => if (replacement == "") None else Some(replacement)
      case None => None
    }

  // volume

  private def volume: Parser[CliVolume] = "vol" ~ rep1(arg) ^^
    {
      case _ ~ categoryPatterns =>
        val vol = new CliVolume
        vol.categoryPatterns = categoryPatterns.map(_.r).map(_.toString) // compile patterns
        vol
    }

  // correlate

  private def correlate: Parser[CliCorrelate] = "correlate" ~ arg ~ arg ~ opt(arg) ~ opt(arg ~ opt(arg) ~ opt(long)) ^^ {
    case _ ~ startPattern ~ endPattern ~ cancelPattern ~ None => {
      val correlate = new CliCorrelate
      correlate.startPattern = startPattern
      correlate.endPattern = endPattern
      correlate.cancelPattern = cancelPattern
      correlate
    }
    case _ ~ startPattern ~ endPattern ~ cancelPattern ~ Some(dateExtractionPattern ~ dateFormat ~ maxTimeLimit) => {
      val correlate = new CliCorrelate
      correlate.startPattern = startPattern
      correlate.endPattern = endPattern
      correlate.cancelPattern = cancelPattern
      correlate.dateExtractionPattern = Some(dateExtractionPattern)
      dateFormat map {
        correlate.dateFormat = _
      }
      correlate.maxTimeLimit = maxTimeLimit
      correlate
    }
  }

  // date range

  private def daterange: Parser[CliDateRange] = "daterange" ~ arg ~ opt(arg) ~ opt(arg) ~ dateRangeUntil ^^ {
    case _ ~ dateExtractionPattern ~ dateFormat ~ from ~ until => {
      val dateRange = new CliDateRange
      dateRange.dateExtractionPattern = dateExtractionPattern
      dateFormat map {
        dateRange.dateFormat = _
      }
      dateRange.from = from
      dateRange.until = until
      dateRange
    }
  }

  private def dateRangeUntil: Parser[Option[String]] = opt("->" ~ arg) ^^
    {
      case Some(_ ~ until) => Some(until)
      case None => None
    }

  // common command line arguments

  private final val ARG_BASE_PATTERN = """(?:"[^"]*(?:""[^"]*)*")|[^\s"]+"""

  private def arg: Parser[String] =
    arg()

  private def arg(except: Option[String] = None): Parser[String] = except match {
    case Some(e) => ("(?!" + Pattern.quote(e) + ")(?:" + ARG_BASE_PATTERN + ")").r
    case None => ARG_BASE_PATTERN.r ^^ {
      case a => if (a.startsWith("\"")) a.substring(1, a.length - 1).replace("\"\"", "\"") else a
    }
  }
  private def int: Parser[Int] = """\d+""".r ^^
    { _.toInt }

  private def long: Parser[Long] = """\d+""".r ^^
    { _.toLong }
}
