package de.fromm.shreddit.cli

/**
 * docme: AddPipe
 *
 * @author Stefan Fromm
 */
case class AddPipe(val name: String) extends CliCommand {
  private var success = false

  //todo: define event boundaries

  def exec(pipes: CliPipes, cli: CliCallback) {
    success = false // in case of several calls to this command
    if (pipes.addPipe(name).isDefined) {
      success = true
      cli.setPipe(pipes.currentPipe.map(_.name))
      cli.showMessages("Added pipe \"%s\".".format(name))
    } else
      cli.showMessages(CliMessage.error("Pipe \"%s\" not definable.".format(name)))
  }

  override def back = if (success) Some(RemovePipe(name)) else None
}
