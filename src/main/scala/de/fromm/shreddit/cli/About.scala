package de.fromm.shreddit.cli

import de.fromm.shreddit.lines.ResourceIn
import java.io.IOException
import java.util.jar.Manifest

/**
 * docme: About
 *
 * @author Stefan Fromm
 */
case class About() extends CliCommand with MessageLoader {
  private def loadVersion: String = {
    var version = "?"
    try {
      val manifestStream = classOf[About].getResourceAsStream("/META-INF/MANIFEST.MF")
      if (manifestStream != null) {
        version = new Manifest(manifestStream).getMainAttributes.getValue("Shreddit-Version")
        if (version == null)
          version = "?"
      }
    } catch {
      case e: IOException =>
    }
    version
  }

  def exec(pipes: CliPipes, cli: CliCallback) {
    val version = loadVersion
    cli.showMessages(loadResource(
      "help/topics/about",
      "There is no about page.").map(m => new CliMessage(m.content.replace("<version>", version), m.level)))
  }
}