package de.fromm.shreddit.cli

import com.googlecode.lanterna.gui.Interactable
import com.googlecode.lanterna.input.Key
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.datatransfer.DataFlavor

/**
 * docme: CliCopyable
 *
 * @author Stefan Fromm
 */
trait CliCopyable extends CliTextInteractable {

  def getText: String

  abstract override def keyboardInteraction(key: Key): Interactable.Result = {
    if (isCtrl('c', key) || isCtrl(Key.Kind.Insert, key)) {
      Toolkit.getDefaultToolkit.getSystemClipboard.setContents(new StringSelection(getText), null)
      Interactable.Result.EVENT_HANDLED
    } else
      super.keyboardInteraction(key)
  }
}