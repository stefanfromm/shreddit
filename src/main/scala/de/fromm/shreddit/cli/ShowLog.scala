package de.fromm.shreddit.cli

/**
 * docme: ShowLog
 *
 * @author Stefan Fromm
 */
case class ShowLog() extends CliCommand {

  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.bringLogToFront
  }
}