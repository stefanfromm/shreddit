package de.fromm.shreddit.cli

import de.fromm.shreddit.pipe.Pipe

/**
 * docme: Cli
 *
 * @author Stefan Fromm
 */
trait CliCallback {
  def commandHistory: List[CliCommand]
  def commandFuture: List[CliCommand]

  def setPipe(name: Option[String])
  def showMessages(messages: CliMessage*)
  def showMessages(messages: List[CliMessage])
  def appendMessages(messages: CliMessage*)
  def appendMessages(messages: List[CliMessage])

  def bringLogToFront

  def quit
}