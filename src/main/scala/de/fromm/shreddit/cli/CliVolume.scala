package de.fromm.shreddit.cli

import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.shredders.Volume

/**
 * docme: CliVolume
 *
 * @author Stefan Fromm
 */
class CliVolume extends CliShredder("volume") {
  var categoryPatterns: List[String] = Nil

  def toShredder(options: PipeOptions): Shredder =
    new Volume(categoryPatterns.map(options.eval(_)))
}