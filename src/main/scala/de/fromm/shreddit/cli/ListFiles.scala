package de.fromm.shreddit.cli

import java.io.File

/**
 * docme: ListFiles
 *
 * @author Stefan Fromm
 */
case class ListFiles() extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    val currentDir = new File(".").getCanonicalFile
    cli.showMessages("Files in working directory %s:".format(currentDir.getAbsolutePath), "")

    for (file <- currentDir.listFiles)
      cli.appendMessages("  %s%s".format(file.getName, if (file.isDirectory) "/" else ""))
  }
}