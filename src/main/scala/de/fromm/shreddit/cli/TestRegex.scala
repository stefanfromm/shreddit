package de.fromm.shreddit.cli

import java.util.regex.PatternSyntaxException
import scala.util.matching.Regex
import de.fromm.shreddit.util.RegexUtil

/**
 * docme: TestRegex
 *
 * @author Stefan Fromm
 */
case class TestRegex(val regex: String, val text: String) extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.showMessages("Regular expression test of \"%s\"".format(regex), "")
    cli.appendMessages("Text    : %s".format(text))

    try {
      val one = RegexUtil.oneOccurrenceOf(regex)
      val multi = RegexUtil.multiOccurrencesOf(regex)
      cli.appendMessages("Valid   : yes")

      val matcher = one.pattern.matcher(text)
      if (matcher.matches) {
        cli.appendMessages("Matches : yes")
        for (group <- 0 to matcher.groupCount)
          cli.appendMessages("Group %2d: %s".format(group, matcher.group(group)))
      } else
        cli.appendMessages("Matches : no")

    } catch {
      case e: PatternSyntaxException =>
        cli.appendMessages(CliMessage.error("Valid   : no"))
    }
  }
}