package de.fromm.shreddit.cli

/**
 * docme: Command; not repeatable
 *
 * @author Stefan Fromm
 */
trait CliCommand {

  //todo: keep original command line or provide user-friendly toString() methods? 
  //todo: implement lastSuccess here (field readable/writable for subclasses, default resetting it to false before exec())

  def exec(pipes: CliPipes, cli: CliCallback)

  def back: Option[CliCommand] =
    None
}
