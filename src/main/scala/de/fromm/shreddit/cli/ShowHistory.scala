package de.fromm.shreddit.cli

/**
 * docme: ShowHistory
 *
 * @author Stefan Fromm
 */
case class ShowHistory() extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.showMessages("Command history:", "")

    val allCommands = (cli.commandHistory ++ cli.commandFuture).filter(_.back.isDefined)
    if (allCommands.isEmpty)
      cli.appendMessages("(no commands executed)")
    else {
      cli.appendMessages("%d commands executed".format(allCommands.size), "")
      var counter = 1
      allCommands.foreach(cmd => {
        //todo: lpadding counter values to maximum number of digits with spaces
        if (counter == cli.commandHistory.length)
          cli.appendMessages("%d. %s <- you are here now".format(counter, cmd))
        else
          cli.appendMessages("%d. %s".format(counter, cmd))
        counter += 1
      })
    }

  }
}