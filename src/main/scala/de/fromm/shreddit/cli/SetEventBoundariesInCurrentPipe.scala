package de.fromm.shreddit.cli

import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries

/**
 * docme: DefineEventBoundaries
 *
 * @author Stefan Fromm
 */
case class SetEventBoundariesInCurrentPipe(val boundaries: EventBoundaries) extends CliCommand {
  require(boundaries != null)

  private var _oldBoundaries = Option.empty[EventBoundaries] // Some = success, None = failure

  def exec(pipes: CliPipes, cli: CliCallback) {
    pipes.currentPipe match {
      case Some(currentPipe) => {
        _oldBoundaries = Some(currentPipe.setEventBoundaries(boundaries))
        cli.showMessages("Set event boundaries in pipe %s.".format(currentPipe.name))
      }
      case None =>
        cli.showMessages(CliMessage.error("Please select a current pipe first!"))
    }
  }

  override def back = if (_oldBoundaries.isDefined) Some(SetEventBoundariesInCurrentPipe(_oldBoundaries.get)) else None
}