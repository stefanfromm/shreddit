package de.fromm.shreddit.cli

/**
 * docme: UnsetConstant
 *
 * @author Stefan Fromm
 */
case class UnsetConstant(val constName: String) extends CliCommand {
  private var _oldValue: Option[String] = None

  def exec(pipes: CliPipes, cli: CliCallback) {
    _oldValue = pipes.removeConstant(constName)
  }

  override def back: Option[CliCommand] = _oldValue match {
    case Some(v) => Some(new SetConstant(constName, v))
    case None => None
  }
}