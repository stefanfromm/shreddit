package de.fromm.shreddit.cli

/**
 * docme: SetConstant
 *
 * @author Stefan Fromm
 */
case class SetConstant(val constName: String, val constValue: String) extends CliCommand {
  private var _oldValue: Option[String] = None

  def exec(pipes: CliPipes, cli: CliCallback) {
    _oldValue = pipes.setConstant(constName, constValue)
  }

  override def back: Option[CliCommand] = _oldValue match {
    case Some(v) => Some(new SetConstant(constName, v))
    case None => Some(new UnsetConstant(constName))
  }
}