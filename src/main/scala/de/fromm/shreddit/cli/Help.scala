package de.fromm.shreddit.cli

import scala.util.parsing.combinator.Parsers
import de.fromm.shreddit.lines.ResourceIn
import de.fromm.shreddit.lines.ResourceIn

/**
 * docme: Help
 *
 * @author Stefan Fromm
 */
case class Help(val reason: Option[CliMessage], val command: Option[String]) extends CliCommand with MessageLoader {
  def exec(pipes: CliPipes, cli: CliCallback) {
    //todo: get help from parser syntax or from parser failures, e.g. unknown command, unknown option
    cli.showMessages(
      reason match {
        case Some(msg) => List(msg) ++ List(CliMessage("")) ++ loadHelpPage
        case None => loadHelpPage
      })
  }

  private def loadHelpPage: List[CliMessage] = {
    loadResource(
      "help/" +
        (command match {
          case Some(c) => "topics/" + c
          case None => "help"
        }),
      if (command.isDefined)
        "There is no help page for command %s.".format(command.get)
      else
        "There is no common help page.")
  }
}

/**
 * @author Stefan Fromm
 */
object Help {
  def apply(reason: String, problematicCommand: String) = new Help(Some(CliMessage(reason)), Some(problematicCommand))
  def apply(reason: String) = new Help(Some(CliMessage(reason)), None)
  def apply() = new Help(None, None)
}