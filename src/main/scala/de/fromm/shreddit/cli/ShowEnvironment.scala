package de.fromm.shreddit.cli

import java.nio.charset.Charset
import scala.collection.JavaConversions
import de.fromm.shreddit.lines.LineBreak
import java.io.File

/**
 * docme: Environment
 *
 * @author Stefan Fromm
 */
case class ShowEnvironment() extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.showMessages("Working directory : %s".format(new File(".").getCanonicalPath))
    cli.appendMessages("Default encoding  : %s a.k.a %s".format(Charset.defaultCharset.name, JavaConversions.asScalaSet(Charset.defaultCharset.aliases).mkString("", ", ", "")))
    cli.appendMessages("Default line break: %s".format(LineBreak.detect(System.getProperty("line.separator")).name))
  }
}