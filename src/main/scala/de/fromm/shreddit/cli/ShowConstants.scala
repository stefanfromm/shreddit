package de.fromm.shreddit.cli

/**
 * docme: ShowConstants
 *
 * @author Stefan Fromm
 */
case class ShowConstants() extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.showMessages("Defined constants:", "")

    if (pipes.constants.isEmpty)
      cli.appendMessages("(no constants)")
    else {
      val resolved = ConstVal.resolve(pipes.constants)

      val maxNameLength = (pipes.constants.keys ++ List(ShowConstants.NAME)).foldLeft(0)((curMax, name) => Math.max(curMax, name.length))
      val maxValueLength = (pipes.constants.values ++ List(ShowConstants.DEFINED_VALUE)).foldLeft(0)((curMax, value) => Math.max(curMax, value.length))
      val maxResolvedValueLength = (resolved.values ++ List(ShowConstants.RESOLVED_VALUE)).foldLeft(0)((curMax, value) => Math.max(curMax, value.length))

      cli.appendMessages(rpad(ShowConstants.NAME, maxNameLength, " ") + " | " + rpad(ShowConstants.DEFINED_VALUE, maxValueLength, " ") + " | " + rpad(ShowConstants.RESOLVED_VALUE, maxResolvedValueLength, " "))
      cli.appendMessages(rpad("", maxNameLength, "-") + "-+-" + rpad("", maxValueLength, "-") + "-+-" + rpad("", maxResolvedValueLength, "-"))
      for ((name, value) <- pipes.constants) {
        var resolvedValue = resolved(name)
        if (value == resolvedValue)
          resolvedValue = ""
        cli.appendMessages(rpad(name, maxNameLength, " ") + " | " + rpad(value, maxValueLength, " ") + " | " + rpad(resolvedValue, maxResolvedValueLength, " "))
      }
    }
  }

  private def rpad(s: String, len: Int, appendix: String): String = {
    var padded = s
    while (padded.length < len)
      padded = padded + appendix
    padded
  }
}

object ShowConstants {
  val NAME = "Name"
  val DEFINED_VALUE = "Defined value"
  val RESOLVED_VALUE = "Resolved value"
}