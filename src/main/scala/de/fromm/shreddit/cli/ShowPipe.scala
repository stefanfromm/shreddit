package de.fromm.shreddit.cli

import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.MarkerEventBoundaries

/**
 * docme: ShowPipe
 *
 * @author Stefan Fromm
 */
case class ShowPipe(val name: Option[String]) extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    name match {
      case Some(name) => {
        pipes.pipeByName(name) match {
          case Some(pipe) =>
            showPipe(pipe, cli)
          case None => {
            cli.showMessages(CliMessage.error("Pipe \"%s\" does not exist. Defined are:".format(name)), "")
            ShowPipes.appendPipes(pipes, cli)
          }
        }
      }
      case None => {
        pipes.currentPipe match {
          case Some(pipe) =>
            showPipe(pipe, cli)
          case None =>
            cli.showMessages(CliMessage.error("Please select a current pipe first!"))
        }
      }
    }
  }

  private def showPipe(pipe: CliPipe, cli: CliCallback) {
    cli.showMessages("Details of pipe \"%s\":".format(pipe.name), "")

    cli.appendMessages("Event detection: %s".format(pipe.eventBoundaries match {
      case OneToOneEventBoundaries() => "event per line"
      case MarkerEventBoundaries(startPattern, maxLines) => "event marker \"%s\", max. %d lines".format(startPattern.toString, maxLines)
    }), "")

    if (pipe.steps.isEmpty)
      cli.appendMessages("(no steps)")
    else {
      var counter = 1
      pipe.steps.foreach(s => {
        //todo: lpadding counter values to maximum number of digits with spaces
        cli.appendMessages("%d. \"%s\" %s".format(counter, s.name, s.shredder.id))
        counter += 1
      })
    }
  }
}