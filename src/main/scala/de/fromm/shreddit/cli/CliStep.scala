package de.fromm.shreddit.cli

import de.fromm.shreddit.pipe.Step

/**
 * docme: CliStep
 *
 * @author Stefan Fromm
 */
class CliStep(val name: String, val shredder: CliShredder) {

  def toStep(options: PipeOptions): Step = {
    new Step(name, shredder.toShredder(options))
  }
}