package de.fromm.shreddit.cli

/**
 * docme: AddPipe
 *
 * @author Stefan Fromm
 */
case class UsePipe(val name: String) extends CliCommand {
  private var oldName: String = null

  def exec(pipes: CliPipes, cli: CliCallback) {
    val pipe = pipes.currentPipe = name
    if (pipe.isDefined) {
      oldName = name
      cli.setPipe(pipe.map(_.name))
      cli.showMessages("Switched to pipe \"%s\".".format(name))
    } else {
      cli.showMessages(CliMessage.error("Pipe \"%s\" does not exist. Defined are:".format(name)), "")
      ShowPipes.appendPipes(pipes, cli)
    }
  }

}