package de.fromm.shreddit.cli

import com.googlecode.lanterna.gui.Interactable
import com.googlecode.lanterna.input.Key

/**
 * docme: CliTextInteractable
 *
 * @author Stefan Fromm
 */
trait CliTextInteractable extends Interactable {
  protected[cli] def isCtrl(c: Char, key: Key) =
    key.getKind == Key.Kind.NormalKey && key.getCharacter == c && key.isCtrlPressed && !key.isAltPressed

  protected[cli] def isCtrl(kind: Key.Kind, key: Key) =
    key.getKind == kind && key.isCtrlPressed && !key.isAltPressed
}