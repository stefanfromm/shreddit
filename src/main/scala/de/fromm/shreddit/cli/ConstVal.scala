package de.fromm.shreddit.cli

import scala.util.parsing.combinator.RegexParsers

/**
 * docme: ConstValue
 *
 * @author Stefan Fromm
 */
sealed trait ConstVal {
  def eval(placeholders: Map[String, String]): String

  def needsResolution: Boolean
}

/**
 * @author Stefan Fromm
 */
case class ConstExpression(val start: String, val expr: ConstVal, val end: String) extends ConstVal {
  def eval(placeholders: Map[String, String]): String = {
    val v = expr.eval(placeholders)
    placeholders.get(v) match {
      case Some(replaced) => replaced
      case None => start + v + end
    }
  }

  def needsResolution = true

  override def toString = start + expr + end
}

/**
 * @author Stefan Fromm
 */
case class ConstLiteral(val value: String) extends ConstVal {
  def eval(placeholders: Map[String, String]): String =
    value

  def needsResolution = false

  override def toString = value
}

/**
 * @author Stefan Fromm
 */
case class ConstSequence(val constValues: List[ConstVal]) extends ConstVal {
  def eval(placeholders: Map[String, String]): String =
    constValues.map(_.eval(placeholders)).mkString("", "", "")

  def needsResolution = constValues.filter(_.needsResolution).size > 0

  override def toString = constValues.mkString("", "", "")
}

/**
 * @author Stefan Fromm
 */
object ConstVal extends RegexParsers {
  def eval(value: String, placeholders: Map[String, String]): String =
    resolveValue(value, resolve(placeholders))

  def unresolved(placeholders: Map[String, String]): Set[String] = {
    def needsResolution(v: String): Boolean = parse(v) match {
      case Some(expr) => expr.needsResolution
      case _ => false
    }

    resolve(placeholders).values.filter(needsResolution(_)).toSet
  }

  def resolve(placeholders: Map[String, String]): Map[String, String] = {
    var last = Map.empty[String, String]
    var current = placeholders
    while (last != current) {
      var newCurrent = current
      for ((key, value) <- current) {
        val resolvedValue = resolveValue(value, newCurrent)
        if (resolvedValue != value)
          newCurrent = newCurrent ++ Map(key -> resolvedValue)
      }
      last = current
      current = newCurrent
    }

    //todo: what if place holders are never resolvable? -> error

    current
  }

  private def resolveValue(v: String, placeholders: Map[String, String]): String = {
    //todo: detect bidirectional references
    val parsed = parse(v) match {
      case Some(expr) => expr
      case _ => ConstLiteral(v)
    }
    parsed.eval(placeholders)
  }

  private def parse(v: String): Option[ConstVal] =
    parseAll(const, v) match {
      case Success(result, _) => Some(result)
      case NoSuccess(msg, _) => { println(msg); None }
    }

  // parsing rules

  private def bslash: Parser[ConstLiteral] = """\\""" ^^ {
    _ => ConstLiteral("""\""")
  }

  private def dollar: Parser[ConstLiteral] = """\$""" ^^ {
    _ => ConstLiteral("$")
  }

  private def openingBrace: Parser[ConstLiteral] = """\{""" ^^ {
    _ => ConstLiteral("{")
  }

  private def closingBrace: Parser[ConstLiteral] = """\}""" ^^ {
    _ => ConstLiteral("}")
  }

  private def escaped: Parser[ConstVal] = bslash | dollar | openingBrace | closingBrace

  private def expr: Parser[ConstExpression] = "${" ~! rep1(term) ~! "}" ^^ {
    case _ ~ values ~ _ =>
      if (values.length == 1)
        ConstExpression("${", values(0), "}")
      else
        ConstExpression("${", ConstSequence(values), "}")
  }

  private def literal: Parser[ConstLiteral] = """[^\\${}]+""".r ^^ {
    ConstLiteral(_)
  }

  private def term: Parser[ConstVal] = escaped | expr | literal

  private def const: Parser[ConstVal] = rep1(term) ^^ {
    case values => {
      if (values.length == 1)
        values(0)
      else
        ConstSequence(values)
    }
  }
}