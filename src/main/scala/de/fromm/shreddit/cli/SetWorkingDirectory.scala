package de.fromm.shreddit.cli

import java.io.File

/**
 * docme: SetWorkingDirectory
 *
 * @author Stefan Fromm
 */
case class SetWorkingDirectory(val directoryName: String) extends CliCommand {
  private var _oldDirectoryName = Option.empty[String]

  def exec(pipes: CliPipes, cli: CliCallback) {
    // source: http://stackoverflow.com/questions/840190/changing-the-current-working-directory-in-java
    val directory = new File(directoryName).getCanonicalFile
    if (directory.exists && directory.isDirectory ) {
      val oldUserDir = System.setProperty("user.dir", directory.getAbsolutePath)
      if (oldUserDir != null) {
        _oldDirectoryName = Some(oldUserDir)
        cli.showMessages("Set working directory to %s.".format(new File(".").getCanonicalPath))
      } else
        cli.showMessages(CliMessage.error("Could not set working directory."))
    } else
      cli.showMessages(CliMessage.error("Directory %s does not exist.".format(directoryName)))
  }

  override def back: Option[CliCommand] =
    if (_oldDirectoryName.isDefined)
      Some(SetWorkingDirectory(_oldDirectoryName.get))
    else
      None
}