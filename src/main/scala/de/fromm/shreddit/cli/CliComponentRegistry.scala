package de.fromm.shreddit.cli

import de.fromm.shreddit.persist.v1.XmlStorageServiceComponent
import de.fromm.shreddit.shredders.Grep

/**
 * docme: CliComponentRegistry
 *
 * @author Stefan Fromm
 *
 */
object CliComponentRegistry extends XmlStorageServiceComponent {
  val storageService = new XmlStorageService
}