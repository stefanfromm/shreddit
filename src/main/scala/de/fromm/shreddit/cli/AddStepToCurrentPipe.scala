package de.fromm.shreddit.cli

/**
 * docme: AddStep
 *
 * @author Stefan Fromm
 */
case class AddStepToCurrentPipe(val stepName: String, val shredder: CliShredder) extends CliCommand {
  private var success = false

  def exec(pipes: CliPipes, cli: CliCallback) {
    success = false // in case of several calls to this command
    pipes.currentPipe match {
      case Some(currentPipe) => {
        if (currentPipe.addStep(stepName, shredder).isDefined) {
          success = true
          cli.showMessages("Step \"%s\" with shredder \"%s\" added in pipe \"%s\".".format(stepName, shredder.id, currentPipe.name))
        } else
          cli.showMessages(CliMessage.error("Step \"%s\" not definable in pipe \"%s\".".format(stepName, currentPipe.name)))
      }
      case None =>
        cli.showMessages(CliMessage.error("Please select a current pipe first!"))
    }
  }

  override def back = if (success) Some(RemoveStepFromCurrentPipe(stepName)) else None
}