package de.fromm.shreddit.cli

import java.io.File

/**
 * docme: SavePipes
 *
 * @author Stefan Fromm
 */
case class SavePipes(val targetFile: Option[File]) extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    CliComponentRegistry.storageService.savePipes(pipes, targetFile)
    //    //todo: if file is not given then ask user whether to store in user home or in working directory
    //    val realTargetFile = targetFile match {
    //      case Some(file) => file
    //      case None => new File("shreddit-config.xml")
    //    }
    cli.showMessages("Saved %d pipes and %d constants.".format(pipes.pipes.length, pipes.constants.size), "")
  }
}