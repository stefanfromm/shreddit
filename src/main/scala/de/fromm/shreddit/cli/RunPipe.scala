package de.fromm.shreddit.cli

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fromm.shreddit.io.TextFile
import de.fromm.shreddit.shredders.Progress

/**
 * docme: RunPipe
 *
 * @author Stefan Fromm
 */
case class RunPipe(val name: Option[String], in: List[TextFile], out: Option[TextFile]) extends CliCommand {
  private final val LOG = LoggerFactory.getLogger(classOf[RunPipe])

  //todo: make out file optional, in this case send events to nirvana = /dev/nul
  def exec(pipes: CliPipes, cli: CliCallback) {
    name match {
      case Some(name) => {
        pipes.pipeByName(name) match {
          case Some(pipe) =>
            runPipe(pipe, pipes, cli)
          case None => {
            cli.showMessages(CliMessage.error("Pipe \"%s\" does not exist. Defined are:".format(name)), "")
            ShowPipes.appendPipes(pipes, cli)
          }
        }
      }
      case None => {
        pipes.currentPipe match {
          case Some(pipe) =>
            runPipe(pipe, pipes, cli)
          case None =>
            cli.showMessages(CliMessage.error("Please select a current pipe first!"))
        }
      }
    }
  }

  private def runPipe(pipe: CliPipe, pipes: CliPipes, cli: CliCallback) {
    if (pipe.steps.isEmpty)
      cli.showMessages("Pipe \"%s\" is empty. Please define steps first.".format(pipe.name), "")
    else {
      val options = new PipeOptions {
        val constants = pipes.constants
      }

      val listener = new Progress.Listener {
        def absoluteProgress(currentVolume: Long) {

        }

        def relativeProgress(currentPercent: Int) {
          LOG.info("Finished %d percent.".format(currentPercent))
        }
      }

      pipe.toPipe(in, out, options, listener) match {
        case (p, inCounter, outCounter) => {
          LOG.info("Starting pipe \"{}\"...", pipe.name)
          val t0 = System.currentTimeMillis
          p.run
          val t1 = System.currentTimeMillis
          LOG.info("Ran pipe \"%s\" in %d ms. Processed %d lines.".format(pipe.name, t1 - t0, inCounter.lineCount))
        }
      }
    }
  }
}