package de.fromm.shreddit.cli

import java.io.File

/**
 * docme: LoadPipes
 *
 * @author Stefan Fromm
 */
case class LoadPipes(val sourceFile: Option[File]) extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    val newPipes = CliComponentRegistry.storageService.loadPipes(sourceFile)

    pipes.clear

    for ((name, value) <- newPipes.constants)
      pipes.setConstant(name, value)

    newPipes.pipes.foreach(p => {
      val newPipe = pipes.addPipe(p.name).get
      newPipe.setEventBoundaries(p.eventBoundaries)
      p.steps.foreach(s => {
        val newStep = newPipe.addStep(s.name, s.shredder)
        //todo: grep etc.
      })
    })

    if (newPipes.currentPipe.isDefined)
      pipes.currentPipe = newPipes.currentPipe.get.name

    cli.setPipe(pipes.currentPipe.map(_.name))
    cli.showMessages("Loaded %d pipes and %d constants:".format(pipes.pipes.length, pipes.constants.size), "")
    ShowPipes.appendPipes(pipes, cli)
  }
}