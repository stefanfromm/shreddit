package de.fromm.shreddit.cli

import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.shredders.Rewrite

/**
 * docme: CliRewrite
 *
 * @author Stefan Fromm
 */
class CliRewrite extends CliShredder("rewrite") {
  var detectionPattern: Option[String] = None
  var replacePattern: String = null
  var replacement: Option[String] = None

  def toShredder(options: PipeOptions): Shredder =
    new Rewrite(
      detectionPattern.map(options.eval(_)),
      options.eval(replacePattern),
      replacement.map(options.eval(_)))
}