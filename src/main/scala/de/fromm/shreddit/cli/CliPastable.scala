package de.fromm.shreddit.cli

import com.googlecode.lanterna.gui.Interactable
import com.googlecode.lanterna.input.Key
import java.awt.Toolkit
import java.awt.datatransfer.DataFlavor

/**
 * docme: CliPastable
 *
 * @author Stefan Fromm
 */
trait CliPastable extends CliTextInteractable {

  def setText(t: String)

  abstract override def keyboardInteraction(key: Key): Interactable.Result = {
    if (isCtrl('v', key)) /* Shift+Insert not detectable */ {
      setText(Toolkit.getDefaultToolkit.getSystemClipboard.getData(DataFlavor.stringFlavor).asInstanceOf[String])
      Interactable.Result.EVENT_HANDLED
    } else
      super.keyboardInteraction(key)
  }

}