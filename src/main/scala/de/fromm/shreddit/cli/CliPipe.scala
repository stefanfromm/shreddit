package de.fromm.shreddit.cli

import de.fromm.shreddit.pipe.Pipe
import de.fromm.shreddit.io.TextFile
import de.fromm.shreddit.shredders.LineCounter
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.EventBoundaries
import de.fromm.shreddit.shredders.Progress

/**
 * docme: CliPipe
 *
 * @author Stefan Fromm
 */
class CliPipe(val name: String) {
  private var _steps = List.empty[CliStep]
  private var _eventBoundaries: EventBoundaries = new OneToOneEventBoundaries

  def eventBoundaries = _eventBoundaries

  def setEventBoundaries(eb: EventBoundaries): EventBoundaries = {
    require(eb != null)
    val old = _eventBoundaries
    _eventBoundaries = eb
    old
  }

  def steps = _steps

  def stepByName(name: String): Option[CliStep] =
    _steps.find(_.name == name)

  def addStep(name: String, shredder: CliShredder): Option[CliStep] = {
    _steps.find(_.name == name) match {
      case Some(_) => None
      case None => {
        val step = new CliStep(name, shredder)
        _steps = _steps ++ List(step)
        Some(step)
      }
    }
  }

  def removeStep(name: String): Option[CliStep] =
    stepByName(name) match {
      case Some(step) => {
        _steps = _steps diff List(step)
        Some(step)
      }
      case None => None
    }

  def toPipe(in: List[TextFile], out: Option[TextFile], options: PipeOptions, listener: Progress.Listener): (Pipe, LineCounter, LineCounter) = {
    var p = new Pipe(name, in, out).withEventBoundaries(_eventBoundaries)

    val progress = new Progress(in.foldLeft(0L)((sum, txtFile) => sum + txtFile.volume), listener)
    val inCounter = new LineCounter
    val outCounter = new LineCounter
    p = p.addStep("progress", progress)
    p = p.addStep("inCount", inCounter)
    steps.foreach(s => {
      p = p.addStep(s.toStep(options))
    })
    p = p.addStep("outCount", outCounter)
    (p, inCounter, outCounter)
  }

  //todo: validate pipe against current pipe options for making sure values are valid after replacing them with constants
}
