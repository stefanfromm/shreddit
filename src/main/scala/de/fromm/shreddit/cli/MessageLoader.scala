package de.fromm.shreddit.cli

import de.fromm.shreddit.lines.ResourceIn

/**
 * docme: ResourceLoadingCommand
 *
 * @author Stefan Fromm
 */
trait MessageLoader {
  protected def loadResource(relativePath: String, noContentMessage: String): List[CliMessage] = {
    val url = "/" + classOf[Help].getPackage.getName.replace('.', '/') + "/" + relativePath

    val in = new ResourceIn(url)

    var text = List.empty[CliMessage]
    try {
      var line = in.read
      while (line.isDefined) {
        text = text ++ List(CliMessage(line.get.content))
        line = in.read
      }
    } finally {
      in.close
    }

    if (text.isEmpty)
      text = List(CliMessage(noContentMessage))

    text
  }
}