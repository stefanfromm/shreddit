package de.fromm.shreddit.cli

/**
 * @author Stefan Fromm
 */
case class Welcome() extends CliCommand with MessageLoader {
  def exec(pipes: CliPipes, cli: CliCallback) {
    //todo: get help from parser syntax or from parser failures, e.g. unknown command, unknown option
    cli.showMessages(loadResource("welcome", ""))
  }
}