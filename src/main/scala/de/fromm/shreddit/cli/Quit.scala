package de.fromm.shreddit.cli

/**
 * docme: Quit
 *
 * @author Stefan Fromm
 */
case class Quit() extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.quit
  }
}
