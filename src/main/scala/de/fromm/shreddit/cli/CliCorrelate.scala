package de.fromm.shreddit.cli

import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.shredders.Correlate
import de.fromm.shreddit.shredders.DateExtractor
import org.joda.time.format.DateTimeFormat

/**
 * @author Stefan Fromm
 */
class CliCorrelate extends CliShredder("correlate") {
  var startPattern: String = null
  var endPattern: String = null
  var cancelPattern: Option[String] = None
  var dateExtractionPattern: Option[String] = None
  var dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS"
  var maxTimeLimit: Option[Long] = None

  def toShredder(options: PipeOptions): Shredder =
    new Correlate(
      options.eval(startPattern),
      options.eval(endPattern),
      cancelPattern.map(options.eval(_)),
      dateExtractionPattern.map(p => 
        new DateExtractor(options.eval(p), options.eval(dateFormat))),
      maxTimeLimit,
      true,
      true)

}