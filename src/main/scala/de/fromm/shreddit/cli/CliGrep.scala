package de.fromm.shreddit.cli

import scala.util.matching.Regex
import de.fromm.shreddit.shredders.Grep
import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.util.RegexUtil

/**
 * docme: CliGrep
 *
 * @author Stefan Fromm
 */
class CliGrep extends CliShredder("grep") {
  var pattern: String = null
  var before: Int = 0
  var after: Int = 0
  var reverse: Boolean = false

  def toShredder(options: PipeOptions): Shredder =
    new Grep(options.eval(pattern), before, after, reverse)
}