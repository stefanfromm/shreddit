package de.fromm.shreddit.cli

/**
 * docme: AddPipe
 *
 * @author Stefan Fromm
 */
case class RemovePipe(val name: String) extends CliCommand {
  private var success = false

  def exec(pipes: CliPipes, cli: CliCallback) {
    success = false // in case of several calls to this command
    if (pipes.removePipe(name).isDefined) {
      success = true
      cli.setPipe(None)
      cli.showMessages("Removed pipe \"%s\".".format(name))
    } else
      cli.showMessages(CliMessage.error("Pipe \"%s\" does not exist (anymore).".format(name)))
  }

  override def back = if (success) Some(AddPipe(name)) else None
}
