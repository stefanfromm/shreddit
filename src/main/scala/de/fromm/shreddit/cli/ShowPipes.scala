package de.fromm.shreddit.cli

import de.fromm.shreddit.pipe.OneToOneEventBoundaries
import de.fromm.shreddit.pipe.MarkerEventBoundaries

/**
 * docme: ShowPipes
 *
 * @author Stefan Fromm
 */
case class ShowPipes() extends CliCommand {
  def exec(pipes: CliPipes, cli: CliCallback) {
    cli.showMessages("All currently defined pipes:", "")
    ShowPipes.appendPipes(pipes, cli)
  }
}

/**
 * @author Stefan Fromm
 */
object ShowPipes {
  def appendPipes(pipes: CliPipes, cli: CliCallback) {
    if (pipes.pipes.isEmpty)
      cli.appendMessages("(no pipes)")
    else
      cli.appendMessages(
        pipes.pipes.map(p =>
          CliMessage("%s - %d steps - %s".format(p.name, p.steps.size, p.eventBoundaries match {
            case OneToOneEventBoundaries() => "event per line"
            case MarkerEventBoundaries(_, _) => "marker for events"
          }), CliMessageLevel.Info)).sortBy(_.content))
  }
}