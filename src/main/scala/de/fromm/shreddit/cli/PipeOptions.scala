package de.fromm.shreddit.cli

/**
 * docme: PipeOptions
 *
 * @author Stefan Fromm
 */
trait PipeOptions {
  def constants: Map[String, String]
  
  def eval(value: String): String = ConstVal.eval(value, constants)
}