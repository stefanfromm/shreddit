package de.fromm.shreddit.pipe

import de.fromm.shreddit.events.Event
import de.fromm.shreddit.io.Io
import de.fromm.shreddit.io.IoControl

/**
 * docme: StepChain
 *
 * @author Stefan Fromm
 */
final class StepChain(private val steps: List[Step]) {
  require(steps != null)

  def next(event: Event, io: Io) {
    var nextStepEvents: List[Event] = List(event) // current events being processed
    var restSteps = steps

    while (!restSteps.isEmpty && !nextStepEvents.isEmpty) {
      val currentStep = restSteps.head

      var outEvents: List[Event] = Nil

      nextStepEvents.foreach(e => {
        val ioControl = new DefaultIoControl(e)
        currentStep.next(e, ioControl)
        outEvents = outEvents ++ ioControl.outEvents
      })

      restSteps = restSteps.tail
      nextStepEvents = outEvents
    }

    nextStepEvents.foreach(io.write(_))
  }

  def start(io: Io) {
    val ioControl = new DefaultIoControl
    steps.foreach(s => {
      s.shredder.start(s, ioControl)
    }) //todo: try finally around each shredder
    ioControl.outEvents.foreach(io.write(_))
  }

  def stop(io: Io) {
    val ioControl = new DefaultIoControl
    steps.foreach(_.shredder.stop(ioControl)) //todo: try finally around each shredder
    ioControl.outEvents.foreach(io.write(_))
  }

  /**
   * @author Stefan Fromm
   */
  private class DefaultIoControl private (val event: Option[Event]) extends IoControl {
    private var _outEvents: List[Event] = Nil

    def this() = this(None)
    def this(event: Event) = this(Some(event))

    def outEvents =
      _outEvents

    def passOn() {
      require(event.isDefined)
      passOn(event.get)
    }

    def passOn(event: Event) {
      require(event != null)
      _outEvents = _outEvents ++ List(event)
    }
  }
}