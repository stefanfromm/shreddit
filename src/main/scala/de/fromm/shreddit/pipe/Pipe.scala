package de.fromm.shreddit.pipe

import de.fromm.shreddit.events.EventIn
import de.fromm.shreddit.events.EventOut
import de.fromm.shreddit.io.Io
import de.fromm.shreddit.io.TextFile
import de.fromm.shreddit.io.OpenMode
import de.fromm.shreddit.lines.SysIn
import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.events.MarkerEventIn
import de.fromm.shreddit.events.LineEventIn
import de.fromm.shreddit.lines.LineIn
import de.fromm.shreddit.lines.MultiLineIn

/**
 * docme: Pipe
 *
 * @author Stefan Fromm
 */
final class Pipe private (private val name: String, private val ins: List[TextFile], private val out: Option[TextFile], private val eventBoundaries: EventBoundaries, private val steps: List[Step]) {
  require(name != null)
  require(!name.isEmpty)
  require(ins != null)
  require(out != null)
  require(eventBoundaries != null)
  require(steps != null)

  //todo: add LineOut for statistics
  //todo: normalize in and out to EventIn and EventOut to support any inputs and outputs
  //todo: base Io on EventOut to make it more general
  def this(name: String, ins: List[TextFile]) = this(name, ins, None, new OneToOneEventBoundaries, List.empty[Step])
  def this(name: String, ins: List[TextFile], out: TextFile) = this(name, ins, Some(out), new OneToOneEventBoundaries, List.empty[Step])
  def this(name: String, ins: List[TextFile], out: Option[TextFile]) = this(name, ins, out, new OneToOneEventBoundaries, List.empty[Step])

  def addStep(name: String, shredder: Shredder): Pipe = addStep(new Step(name, shredder))
  def addStep(step: Step): Pipe = new Pipe(name, ins, out, eventBoundaries, steps ++ List(step))

  def withEventBoundaries(boundaries: EventBoundaries): Pipe = {
    require(boundaries != null)
    new Pipe(name, ins, out, boundaries, steps)
  }

  def run {
    val io = new Io
    try {
      val stepChain = new StepChain(steps)
      try {
        stepChain.start(io)

        out match {
          case Some(textFile) => io.switchToFile(textFile, OpenMode.Replace)
          case _ =>
        }

        val eventIn = eventBoundaries.createEventIn(ins match {
          case Nil => SysIn
          case List(singleIn) => singleIn.openForReading
          case multiIns => new MultiLineIn(multiIns.map(_.openForReading)) //todo: lazy opening of files
        })

        try {
          var e = eventIn.read
          while (e.isDefined) {
            stepChain.next(e.get, io)
            e = eventIn.read
          }
        } finally {
          eventIn.close
        }
      } finally {
        stepChain.stop(io)
      }
    } finally {
      io.close
    }
  }
}
