package de.fromm.shreddit.pipe

import de.fromm.shreddit.lines.LineIn
import de.fromm.shreddit.events.MarkerEventIn
import de.fromm.shreddit.events.LineEventIn
import de.fromm.shreddit.events.EventIn

/**
 * @author Stefan Fromm
 */
sealed trait EventBoundaries {
  def createEventIn(lineIn: LineIn): EventIn
}

/**
 * @author Stefan Fromm
 */
case class MarkerEventBoundaries(val startPattern: String, val maxEventLength: Int) extends EventBoundaries {
  require(startPattern != null)
  require(!startPattern.isEmpty)
  require(maxEventLength > 0)

  def createEventIn(lineIn: LineIn): EventIn =
    new MarkerEventIn(startPattern, maxEventLength, lineIn)
}

/**
 * @author Stefan Fromm
 */
case class OneToOneEventBoundaries() extends EventBoundaries {
  def createEventIn(lineIn: LineIn): EventIn =
    new LineEventIn(lineIn)
}