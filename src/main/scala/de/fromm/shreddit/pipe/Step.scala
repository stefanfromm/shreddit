package de.fromm.shreddit.pipe

import de.fromm.shreddit.events.EventOut
import de.fromm.shreddit.events.Event
import de.fromm.shreddit.io.Io
import de.fromm.shreddit.shredders.Shredder
import de.fromm.shreddit.io.IoControl

/**
 * docme: Transformer
 *
 * @author Stefan Fromm
 */
final class Step(val name: String, val description: String, val shredder: Shredder) {
  require(name != null)
  require(!name.isEmpty)
  require(description != null)
  require(shredder != null)

  def this(name: String, shredder: Shredder) = this(name, "", shredder)

  def next(event: Event, io: IoControl) {
    shredder.next(event, io)
  }
}