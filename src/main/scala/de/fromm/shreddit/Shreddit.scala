package de.fromm.shreddit

import com.googlecode.lanterna.gui.GUIScreen
import com.googlecode.lanterna.TerminalFacade
import com.googlecode.lanterna.terminal.Terminal
import com.googlecode.lanterna.screen.Screen
import de.fromm.shreddit.cli.CliWindow
import com.googlecode.lanterna.gui.DefaultBackgroundRenderer
import com.googlecode.lanterna.gui.Window
import com.googlecode.lanterna.gui.Theme

/**
 * @author Stefan Fromm
 */
object Shreddit extends App {

  val tui = TerminalFacade.createGUIScreen
  val renderer = new DefaultBackgroundRenderer
  renderer.setShowMemoryUsage(true)
  tui.setBackgroundRenderer(renderer)

  tui.getScreen.startScreen
  tui.showWindow(new CliWindow, GUIScreen.Position.NEW_CORNER_WINDOW) //todo: setup default file location of files

}