package de.fromm.shreddit.io

import java.nio.charset.Charset
import de.fromm.shreddit.util.Enum
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import de.fromm.shreddit.lines.LineOut
import de.fromm.shreddit.lines.Line
import java.io.File
import de.fromm.shreddit.lines.LineIn
import java.io.FileInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import de.fromm.shreddit.lines.LineBreak
import de.fromm.shreddit.lines.ReaderIn
import de.fromm.shreddit.lines.WriterOut

/**
 * docme: TextFile
 *
 * @author Stefan Fromm
 */
final class TextFile(val fileName: String, val charset: Charset) {
  def this(fileName: String, charsetName: String) = this(fileName, Charset.forName(charsetName))

  private val _file = new File(fileName).getCanonicalFile

  def openForReading: LineIn =
    new ReaderIn(new InputStreamReader(new FileInputStream(_file), charset)) {
      private val _volume = volume

      override def estimateVolume: Long =
        _volume
    }

  def openForWriting(mode: OpenMode): LineOut = {
    val fos = new FileOutputStream(new File(fileName).getCanonicalFile, mode match {
      case OpenMode.Append => true
      case OpenMode.Replace => false
    })

    new WriterOut(new OutputStreamWriter(fos, charset))
  }

  def volume =
    _file.length
}