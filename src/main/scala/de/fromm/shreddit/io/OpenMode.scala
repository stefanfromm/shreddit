package de.fromm.shreddit.io

import de.fromm.shreddit.util.Enum

/**
 * docme: OpenMode
 *
 * @author Stefan Fromm
 */
sealed trait OpenMode extends OpenMode.Value

/**
 * docme: OpenMode$
 *
 * @author Stefan Fromm
 */
object OpenMode extends Enum[OpenMode] {
  case object Append extends OpenMode
  case object Replace extends OpenMode
}
