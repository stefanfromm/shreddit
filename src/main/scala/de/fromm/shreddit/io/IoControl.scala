package de.fromm.shreddit.io

import de.fromm.shreddit.events.Event
import de.fromm.shreddit.lines.Line

/**
 * docme: IoControl
 *
 * @author Stefan Fromm
 */
trait IoControl {
  def passOn

  def passOn(event: Event) // for changing or buffering events; may be called several times

  def passOn(ls: List[Line]) {
    passOn(new Event {
      val lines = ls
    })
  }

  def passOn(line: String) {
    passOn(List(Line(line)))
  }

  //todo: multiplex events to several files, switch files, and so on... BUT NO write()
  //todo: get current input TextFile
}