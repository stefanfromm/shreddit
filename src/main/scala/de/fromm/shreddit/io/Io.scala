package de.fromm.shreddit.io

import de.fromm.shreddit.events.EventOut
import de.fromm.shreddit.events.Event
import java.nio.charset.Charset
import de.fromm.shreddit.util.Enum
import java.io.File
import java.io.FileOutputStream
import de.fromm.shreddit.lines.LineOut
import java.io.OutputStreamWriter
import de.fromm.shreddit.lines.Line
import de.fromm.shreddit.lines.SysOut
import de.fromm.shreddit.lines.Nirvana

/**
 * docme: Io
 *
 * @author Stefan Fromm
 */
class Io {
  private var _currentOut: EventOut = EventOut(Nirvana)

  def switchToFile(fileName: String, charset: Charset, openMode: OpenMode) {
    switchToFile(new TextFile(fileName, charset), openMode)
  }

  def switchToFile(file: TextFile, openMode: OpenMode) {
    //improveme: keep file connections open and close them later
    _currentOut.close

    _currentOut = EventOut(file.openForWriting(openMode))
  }

  def switchToSysOut {
    //improveme: keep file connections open and close them later
    _currentOut.close

    _currentOut = EventOut(SysOut)
  }

  def write(event: Event) {
    _currentOut.write(event)
  }

  def close {
    _currentOut.close
  }
}

