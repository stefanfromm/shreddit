package de.fromm.shreddit.lines

import org.scalatest.Suite
import org.junit.Test

/**
 * Test for {@link MultiLineIn}.
 *
 * @author Stefan Fromm
 */
class MultiLineInTest extends Suite {
  @Test
  def singleEmptyLineIn {
    val in = new MultiLineIn(List(new StringIn("")))
    assert(None === in.read)
  }

  @Test
  def twoEmptyLineIns {
    val in = new MultiLineIn(List(new StringIn(""), new StringIn("")))
    assert(None === in.read)
  }

  @Test
  def singleLineInOneLiner {
    val in = new MultiLineIn(List(new StringIn("a")))
    assert("a" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def multiLineInOneLinerAfterEmptyFile {
    val in = new MultiLineIn(List(new StringIn(""), new StringIn("a")))
    assert("a" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def multiLineInOneLinerBeforeEmptyFile {
    val in = new MultiLineIn(List(new StringIn("a"), new StringIn("")))
    assert("a" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def multiLineInOneLinerBetweenEmptyFiles {
    val in = new MultiLineIn(List(new StringIn(""), new StringIn("a"), new StringIn("")))
    assert("a" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def singleLineInTwoLiner {
    val in = new MultiLineIn(List(new StringIn("a\r\nb")))
    assert("a" === in.read.get.content)
    assert("b" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def singleLineInTwoLinerAfterEmptyFile {
    val in = new MultiLineIn(List(new StringIn(""), new StringIn("a\r\nb")))
    assert("a" === in.read.get.content)
    assert("b" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def singleLineInTwoLinerBeforeEmptyFile {
    val in = new MultiLineIn(List(new StringIn("a\r\nb"), new StringIn("")))
    assert("a" === in.read.get.content)
    assert("b" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def singleLineInTwoLinerBetweenEmptyFiles {
    val in = new MultiLineIn(List(new StringIn(""), new StringIn("a\r\nb"), new StringIn("")))
    assert("a" === in.read.get.content)
    assert("b" === in.read.get.content)
    assert(None === in.read)
  }

  @Test
  def complexSequence {
    val in = new MultiLineIn(List(new StringIn(""), new StringIn("a\r\nb"), new StringIn(""), new StringIn(""), new StringIn("c"), new StringIn("d"), new StringIn("\r\ne")))
    assert("a" === in.read.get.content)
    assert("b" === in.read.get.content)
    assert("c" === in.read.get.content)
    assert("d" === in.read.get.content)
    assert("" === in.read.get.content)
    assert("e" === in.read.get.content)
    assert(None === in.read)
  }
}