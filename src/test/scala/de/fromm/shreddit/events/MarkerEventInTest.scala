package de.fromm.shreddit.events

import org.scalatest.Suite
import org.junit.Test
import de.fromm.shreddit.lines.StringIn
import de.fromm.shreddit.lines.LineIn

/**
 * docme: MarkerEventInTest
 *
 * @author Stefan Fromm
 */
class MarkerEventInTest extends Suite {
  @Test
  def noEvents {
    assert(None === new MarkerEventIn("""^\d+\.""", Int.MaxValue, new LineIn {
      def read = None

      def close {
      }
    }).read)
    assert(None === new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("")).read)
  }

  @Test
  def oneSingleLineEvent {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("1. x"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def oneSingleLineEventAfterIgnoredLine {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("ignored\n1. x"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def oneSingleLineEventAfterTwoIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("ignored\nignored\n1. x"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def oneSingleLineEventBeforeIgnoredLine {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("1. x\nignored"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def oneSingleLineEventBeforeTwoIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("1. x\nignored\nignored"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def oneSingleLineEventBetweenSingleIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("ignored\n1. x\nignored"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def oneSingleLineEventBetweenMultipleIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("ignored\nignored\n1. x\nignored\nignored"))

    val event = in.read
    assert(event.isDefined)
    assert(List("1. x") === event.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEvents {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("1. x\n2. x"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsAfterIgnoredLine {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("ignored\n1. x\n2. x"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsAfterTwoIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("ignored\nignored\n1. x\n2. x"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsBeforeIgnoredLine {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("1. x\n2. x\nignored"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsBeforeTwoIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("1. x\n2. x\nignored\nignored"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsBetweenSingleIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("ignored\n1. x\n2. x\nignored"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsBetweenMultipleIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("ignored\nignored\n1. x\n2. x\nignored\nignored"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsMixedWithSingleIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("ignored\n1. x\nignored\n2. x\nignored"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def twoSingleLineEventsMixedWithMultiIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", 1, new StringIn("ignored\nignored\n1. x\nignored\nignored\n2. x\nignored\nignored"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x") === event2.get.lines.map(_.content))

    assert(None === in.read)
  }

  @Test
  def anyLineEventsMixedWithMultiIgnoredLines {
    val in = new MarkerEventIn("""^\d+\.""", Int.MaxValue, new StringIn("ignored\nignored\n1. x\n2. x\ntail of second\n3. x\n4. x\ntail of fourth\n  further tail of fourth"))

    val event1 = in.read
    assert(event1.isDefined)
    assert(List("1. x") === event1.get.lines.map(_.content))

    val event2 = in.read
    assert(event2.isDefined)
    assert(List("2. x", "tail of second") === event2.get.lines.map(_.content))

    val event3 = in.read
    assert(event3.isDefined)
    assert(List("3. x") === event3.get.lines.map(_.content))

    val event4 = in.read
    assert(event4.isDefined)
    assert(List("4. x", "tail of fourth", "  further tail of fourth") === event4.get.lines.map(_.content))

    assert(None === in.read)
  }

}