package de.fromm.shreddit.shredders

import org.scalatest.Suite
import org.junit.Test

import de.fromm.shreddit.pipe.Pipe

import java.io.File

import org.junit.After

import de.fromm.shreddit.io.TextFile
import de.fromm.shreddit.io.OpenMode
import de.fromm.shreddit.lines.Line
import de.fromm.shreddit.lines.LineBreak
import de.fromm.shreddit.pipe.Step

/**
 * Test for {@link LineCounter}.
 *
 * @author Stefan Fromm
 */
class LineCounterTest extends Suite with ShredderSuite {
  @Test
  def noLineOneCounter {
    val out = newTempFile(lines())
    val pipe = new Pipe("test", List(newTempFile(lines())), out).addStep("#1", new LineCounter)
    pipe.run

    assert(List.empty[String] === loadTextFile(out))
  }

  @Test
  def oneLineOneCounter {
    val counter = new LineCounter

    val out = newTempFile(lines())
    val pipe = new Pipe("test", List(newTempFile(lines("line 1"))), out).addStep("#1", counter)
    pipe.run

    assert(List("line 1") === loadTextFile(out))
    assert(1 === counter.lineCount)
  }

  @Test
  def twoLinesOneCounter {
    val counter = new LineCounter

    val out = newTempFile(lines())
    val pipe = new Pipe("test", List(newTempFile(lines("line 1", "line 2"))), out).addStep("#1", counter)
    pipe.run

    assert(List("line 1", "line 2") === loadTextFile(out))
    assert(2 === counter.lineCount)
  }

  @Test
  def noLineTwoCounters {
    val out = newTempFile(lines())
    val pipe = new Pipe("test", List(newTempFile(lines())), out).addStep("#1", new LineCounter).addStep("#2", new LineCounter)
    pipe.run

    assert(List.empty[String] === loadTextFile(out))
  }

  @Test
  def oneLineTwoCounters {
    val counter1 = new LineCounter
    val counter2 = new LineCounter

    val out = newTempFile(lines())
    val pipe = new Pipe("test", List(newTempFile(lines("line 1"))), out).addStep("#1", counter1).addStep("#2", counter2)
    pipe.run

    assert(List("line 1") === loadTextFile(out))
    assert(1 === counter1.lineCount)
    assert(1 === counter2.lineCount)
  }

  @Test
  def twoLinesTwoCounters {
    val counter1 = new LineCounter
    val counter2 = new LineCounter

    val out = newTempFile(lines())
    val pipe = new Pipe("test", List(newTempFile(lines("line 1", "line 2"))), out).addStep("#1", counter1).addStep("#2", counter2)
    pipe.run

    assert(List("line 1", "line 2") === loadTextFile(out))
    assert(2 === counter1.lineCount)
    assert(2 === counter2.lineCount)
  }
}