package de.fromm.shreddit.shredders

import org.scalatest.Suite

import de.fromm.shreddit.pipe.Pipe

import org.junit.Test

/**
 * docme: DateRangeTest
 *
 * @author Stefan Fromm
 */
class DateRangeTest extends Suite with ShredderSuite {
  private def dateRange(dateDetectionPattern: String, dateFormatPattern: String, from: String, until: String, inLines: List[String]) =
    {
      val out = newTempFile(lines())
      val dateExtractor = new DateExtractor(dateDetectionPattern, dateFormatPattern)
      new Pipe("test", List(newTempFile(inLines)), out).addStep("#1", new DateRange(dateExtractor, Some(dateExtractor.dateFormat.parseDateTime(from)), Some(dateExtractor.dateFormat.parseDateTime(until)))).run
      loadTextFile(out)
    }

  private def timeRange(from: String, until: String, inLines: List[String]) =
    dateRange("""^(\d{2}:\d{2}:\d{2})""", "HH:mm:ss", from, until, inLines)

  @Test
  def dateRange {
    assert(lines() === timeRange("09:00:00", "09:59:59", lines("10:00:00 - a")))
    assert(lines("10:00:00 - a") === timeRange("10:00:00", "10:00:00", lines("10:00:00 - a")))

    assert(lines("10:00:00 - a") === timeRange("09:59:59", "10:00:00", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:00:00 - a") === timeRange("10:00:00", "10:00:00", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:00:00 - a") === timeRange("10:00:00", "10:00:01", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:00:00 - a") === timeRange("09:59:59", "10:00:01", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))

    assert(lines("10:01:00 - b") === timeRange("10:00:59", "10:01:00", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:01:00 - b") === timeRange("10:01:00", "10:01:00", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:01:00 - b") === timeRange("10:01:00", "10:01:01", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:01:00 - b") === timeRange("10:00:59", "10:01:01", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))

    assert(lines("10:02:00 - c") === timeRange("10:01:59", "10:02:00", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:02:00 - c") === timeRange("10:02:00", "10:02:00", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:02:00 - c") === timeRange("10:02:00", "10:02:01", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
    assert(lines("10:02:00 - c") === timeRange("10:01:59", "10:02:01", lines("10:00:00 - a", "10:01:00 - b", "10:02:00 - c")))
  }
}