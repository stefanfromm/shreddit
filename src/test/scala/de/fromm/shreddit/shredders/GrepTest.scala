package de.fromm.shreddit.shredders

import org.scalatest.Suite

import scala.util.matching.Regex
import de.fromm.shreddit.pipe.Pipe

import org.junit.Test

/**
 * docme: GrepTest
 *
 * @author Stefan Fromm
 */
class GrepTest extends Suite with ShredderSuite {
  private def lgrep(pattern: String, inLines: List[String]): List[String] =
    grep(pattern, 0, 0, false, inLines)

  private def lgrep(pattern: String, before: Int, after: Int, inLines: List[String]): List[String] =
    grep(pattern, before, after, false, inLines)

  private def rlgrep(pattern: String, inLines: List[String]): List[String] =
    grep(pattern, 0, 0, true, inLines)

  private def rlgrep(pattern: String, before: Int, after: Int, inLines: List[String]): List[String] =
    grep(pattern, before, after, true, inLines)

  private def grep(pattern: String, before: Int, after: Int, reverse: Boolean, inLines: List[String]): List[String] = {
    val out = newTempFile(lines())
    new Pipe("test", List(newTempFile(inLines)), out).addStep("#1", new Grep(pattern, before, after, reverse)).run
    loadTextFile(out)
  }

  // todo:
  // 
  //  private String[] egrep(String detectionPattern, String[] inLines) throws Exception
  //  {
  //    return analyze("grep", new String[] {"-p", detectionPattern, "--logEntryStartPattern", "^>"}, inLines);
  //  }
  //
  //  private String[] egrep(String detectionPattern, int linesBefore, int linesAfter, String[] inLines)
  //      throws Exception
  //  {
  //    return analyze(
  //      "grep",
  //      new String[] {
  //        "-p",
  //        detectionPattern,
  //        "-b",
  //        String.valueOf(linesBefore),
  //        "-a",
  //        String.valueOf(linesAfter),
  //        "--logEntryStartPattern",
  //        "^>"},
  //      inLines);
  //  }

  @Test
  def includeSingleLinesOnly {
    assert(lines() === lgrep(".*", lines()))
    assert(lines("a") === lgrep(".*", lines("a")))
    assert(lines("a", "b") === lgrep(".*", lines("a", "b")))

    assert(lines("a") === lgrep("a", lines("a", "b", "c")))
    assert(lines("b") === lgrep("b", lines("a", "b", "c")))
    assert(lines("c") === lgrep("c", lines("a", "b", "c")))

    assert(lines("a", "c") === lgrep("a|c", lines("a", "b", "c")))
    assert(lines("a", "b") === lgrep("b|a", lines("a", "b", "c")))
  }

  // todo:
  // 
  //  @Test
  //  def includeSingleEntriesOnly
  //  {
  //    assert(lines() === egrep(".*", lines()))
  //    assert(lines() === egrep(".*", lines("a")))
  //    assert(lines(">a") === egrep(".*", lines(">a")))
  //    assert(lines() === egrep(".*", lines("a", "b")))
  //    assert(lines(">a", "b") === egrep(".*", lines(">a", "b")))
  //    assert(lines(">a", ">b") === egrep(".*", lines(">a", ">b")))
  //    assert(lines(">b") === egrep(".*", lines("a", ">b")))
  //
  //    assert(lines() === egrep("a", lines("a", "b", "c")))
  //    assert(lines() === egrep("b", lines("a", "b", "c")))
  //    assert(lines() === egrep("c", lines("a", "b", "c")))
  //
  //    assert(lines(">a", "b", "c") === egrep("a", lines(">a", "b", "c")))
  //    assert(lines(">a", "b", "c") === egrep("b", lines(">a", "b", "c")))
  //    assert(lines(">a", "b", "c") === egrep("c", lines(">a", "b", "c")))
  //
  //    assert(lines() === egrep("a", lines("a", ">b", "c")))
  //    assert(lines(">b", "c") === egrep("b", lines("a", ">b", "c")))
  //    assert(lines(">b", "c") === egrep("c", lines("a", ">b", "c")))
  //
  //    assert(lines() === egrep("a", lines("a", "b", ">c")))
  //    assert(lines() === egrep("b", lines("a", "b", ">c")))
  //    assert(lines(">c") === egrep("c", lines("a", "b", ">c")))
  //
  //    assert(lines(">a") === egrep("a", lines(">a", ">b", "c")))
  //    assert(lines(">b", "c") === egrep("b", lines(">a", ">b", "c")))
  //    assert(lines(">b", "c") === egrep("c", lines(">a", ">b", "c")))
  //
  //    assert(lines(">a", "b") === egrep("a", lines(">a", "b", ">c")))
  //    assert(lines(">a", "b") === egrep("b", lines(">a", "b", ">c")))
  //    assert(lines(">c") === egrep("c", lines(">a", "b", ">c")))
  //
  //    assert(lines() === egrep("a", lines("a", ">b", ">c")))
  //    assert(lines(">b") === egrep("b", lines("a", ">b", ">c")))
  //    assert(lines(">c") === egrep("c", lines("a", ">b", ">c")))
  //
  //    assert(lines(">a") === egrep("a", lines(">a", ">b", ">c")))
  //    assert(lines(">b") === egrep("b", lines(">a", ">b", ">c")))
  //    assert(lines(">c") === egrep("c", lines(">a", ">b", ">c")))
  //
  //    assert(lines() === egrep("a|c", lines("a", "b", "c")))
  //    assert(lines() === egrep("b|a", lines("a", "b", "c")))
  //  }
  //
  @Test
  def includeOneLineBefore {
    assert(lines("a1") === lgrep("a", 1, 0, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1") === lgrep("b", 1, 0, lines("a1", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("c", 1, 0, lines("a1", "b1", "c1")))

    assert(lines("a1", "a2") === lgrep("a", 1, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("a2", "b1") === lgrep("b", 1, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("c", 1, 0, lines("a1", "a2", "b1", "c1")))

    assert(lines("a1") === lgrep("a", 1, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1", "b2") === lgrep("b", 1, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("b2", "c1") === lgrep("c", 1, 0, lines("a1", "b1", "b2", "c1")))

    assert(lines("a1") === lgrep("a", 1, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1") === lgrep("b", 1, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("b1", "c1", "c2") === lgrep("c", 1, 0, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("a1", "a2", "a3") === lgrep("a", 1, 0, in3x3))
    assert(lines("a3", "b1", "b2", "b3") === lgrep("b", 1, 0, in3x3))
    assert(lines("b3", "c1", "c2", "c3") === lgrep("c", 1, 0, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("a1", "c2", "a2", "b3", "a3") === lgrep("a", 1, 0, in3xMixed))
    assert(lines("a1", "b1", "c1", "b2", "c3", "b3") === lgrep("b", 1, 0, in3xMixed))
    assert(lines("b1", "c1", "b2", "c2", "a2", "c3") === lgrep("c", 1, 0, in3xMixed))
  }

  @Test
  def includeTwoLinesBefore {
    assert(lines("a1") === lgrep("a", 2, 0, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1") === lgrep("b", 2, 0, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1", "c1") === lgrep("c", 2, 0, lines("a1", "b1", "c1")))

    assert(lines("a1", "a2") === lgrep("a", 2, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2", "b1") === lgrep("b", 2, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("a2", "b1", "c1") === lgrep("c", 2, 0, lines("a1", "a2", "b1", "c1")))

    assert(lines("a1") === lgrep("a", 2, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1", "b2") === lgrep("b", 2, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("b1", "b2", "c1") === lgrep("c", 2, 0, lines("a1", "b1", "b2", "c1")))

    assert(lines("a1") === lgrep("a", 2, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1") === lgrep("b", 2, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1", "c1", "c2") === lgrep("c", 2, 0, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("a1", "a2", "a3") === lgrep("a", 2, 0, in3x3))
    assert(lines("a2", "a3", "b1", "b2", "b3") === lgrep("b", 2, 0, in3x3))
    assert(lines("b2", "b3", "c1", "c2", "c3") === lgrep("c", 2, 0, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("a1", "b2", "c2", "a2", "c3", "b3", "a3") === lgrep("a", 2, 0, in3xMixed))
    assert(lines("a1", "b1", "c1", "b2", "a2", "c3", "b3") === lgrep("b", 2, 0, in3xMixed))
    assert(lines("a1", "b1", "c1", "b2", "c2", "a2", "c3") === lgrep("c", 2, 0, in3xMixed))
  }

  @Test
  def includeOneLineAfter {
    assert(lines("a1", "b1") === lgrep("a", 0, 1, lines("a1", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("b", 0, 1, lines("a1", "b1", "c1")))
    assert(lines("c1") === lgrep("c", 0, 1, lines("a1", "b1", "c1")))

    assert(lines("a1", "a2", "b1") === lgrep("a", 0, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("b", 0, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("c1") === lgrep("c", 0, 1, lines("a1", "a2", "b1", "c1")))

    assert(lines("a1", "b1") === lgrep("a", 0, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("b1", "b2", "c1") === lgrep("b", 0, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("c1") === lgrep("c", 0, 1, lines("a1", "b1", "b2", "c1")))

    assert(lines("a1", "b1") === lgrep("a", 0, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("b1", "c1") === lgrep("b", 0, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("c1", "c2") === lgrep("c", 0, 1, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("a1", "a2", "a3", "b1") === lgrep("a", 0, 1, in3x3))
    assert(lines("b1", "b2", "b3", "c1") === lgrep("b", 0, 1, in3x3))
    assert(lines("c1", "c2", "c3") === lgrep("c", 0, 1, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("a1", "b1", "a2", "c3", "a3") === lgrep("a", 0, 1, in3xMixed))
    assert(lines("b1", "c1", "b2", "c2", "b3", "a3") === lgrep("b", 0, 1, in3xMixed))
    assert(lines("c1", "b2", "c2", "a2", "c3", "b3") === lgrep("c", 0, 1, in3xMixed))
  }

  @Test
  def includeTwoLinesAfter {
    assert(lines("a1", "b1", "c1") === lgrep("a", 0, 2, lines("a1", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("b", 0, 2, lines("a1", "b1", "c1")))
    assert(lines("c1") === lgrep("c", 0, 2, lines("a1", "b1", "c1")))

    assert(lines("a1", "a2", "b1", "c1") === lgrep("a", 0, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("b", 0, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("c1") === lgrep("c", 0, 2, lines("a1", "a2", "b1", "c1")))

    assert(lines("a1", "b1", "b2") === lgrep("a", 0, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("b1", "b2", "c1") === lgrep("b", 0, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("c1") === lgrep("c", 0, 2, lines("a1", "b1", "b2", "c1")))

    assert(lines("a1", "b1", "c1") === lgrep("a", 0, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines("b1", "c1", "c2") === lgrep("b", 0, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines("c1", "c2") === lgrep("c", 0, 2, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("a1", "a2", "a3", "b1", "b2") === lgrep("a", 0, 2, in3x3))
    assert(lines("b1", "b2", "b3", "c1", "c2") === lgrep("b", 0, 2, in3x3))
    assert(lines("c1", "c2", "c3") === lgrep("c", 0, 2, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("a1", "b1", "c1", "a2", "c3", "b3", "a3") === lgrep("a", 0, 2, in3xMixed))
    assert(lines("b1", "c1", "b2", "c2", "a2", "b3", "a3") === lgrep("b", 0, 2, in3xMixed))
    assert(lines("c1", "b2", "c2", "a2", "c3", "b3", "a3") === lgrep("c", 0, 2, in3xMixed))
  }

  @Test
  def includeOneLineAround {
    assert(lines("a1", "b1") === lgrep("a", 1, 1, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1", "c1") === lgrep("b", 1, 1, lines("a1", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("c", 1, 1, lines("a1", "b1", "c1")))

    assert(lines("a1", "a2", "b1") === lgrep("a", 1, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("a2", "b1", "c1") === lgrep("b", 1, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("b1", "c1") === lgrep("c", 1, 1, lines("a1", "a2", "b1", "c1")))

    assert(lines("a1", "b1") === lgrep("a", 1, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1", "b2", "c1") === lgrep("b", 1, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("b2", "c1") === lgrep("c", 1, 1, lines("a1", "b1", "b2", "c1")))

    assert(lines("a1", "b1") === lgrep("a", 1, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1", "c1") === lgrep("b", 1, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("b1", "c1", "c2") === lgrep("c", 1, 1, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("a1", "a2", "a3", "b1") === lgrep("a", 1, 1, in3x3))
    assert(lines("a3", "b1", "b2", "b3", "c1") === lgrep("b", 1, 1, in3x3))
    assert(lines("b3", "c1", "c2", "c3") === lgrep("c", 1, 1, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("a1", "b1", "c2", "a2", "c3", "b3", "a3") === lgrep("a", 1, 1, in3xMixed))
    assert(lines("a1", "b1", "c1", "b2", "c2", "c3", "b3", "a3") === lgrep("b", 1, 1, in3xMixed))
    assert(lines("b1", "c1", "b2", "c2", "a2", "c3", "b3") === lgrep("c", 1, 1, in3xMixed))
  }

  @Test
  def includeTwoLinesAround {
    assert(lines("a1", "b1", "c1") === lgrep("a", 2, 2, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1", "c1") === lgrep("b", 2, 2, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1", "c1") === lgrep("c", 2, 2, lines("a1", "b1", "c1")))

    assert(lines("a1", "a2", "b1", "c1") === lgrep("a", 2, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2", "b1", "c1") === lgrep("b", 2, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("a2", "b1", "c1") === lgrep("c", 2, 2, lines("a1", "a2", "b1", "c1")))

    assert(lines("a1", "b1", "b2") === lgrep("a", 2, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1", "b2", "c1") === lgrep("b", 2, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("b1", "b2", "c1") === lgrep("c", 2, 2, lines("a1", "b1", "b2", "c1")))

    assert(lines("a1", "b1", "c1") === lgrep("a", 2, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1", "c1", "c2") === lgrep("b", 2, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1", "c1", "c2") === lgrep("c", 2, 2, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("a1", "a2", "a3", "b1", "b2") === lgrep("a", 2, 2, in3x3))
    assert(lines("a2", "a3", "b1", "b2", "b3", "c1", "c2") === lgrep("b", 2, 2, in3x3))
    assert(lines("b2", "b3", "c1", "c2", "c3") === lgrep("c", 2, 2, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3") === lgrep("a", 2, 2, in3xMixed))
    assert(lines("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3") === lgrep("b", 2, 2, in3xMixed))
    assert(lines("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3") === lgrep("c", 2, 2, in3xMixed))
  }

  @Test
  def excludeSingleLinesOnly {
    assert(lines() === rlgrep(".*", lines()))
    assert(lines() === rlgrep(".*", lines("a")))
    assert(lines() === rlgrep(".*", lines("a", "b")))

    assert(lines("b", "c") === rlgrep("a", lines("a", "b", "c")))
    assert(lines("a", "c") === rlgrep("b", lines("a", "b", "c")))
    assert(lines("a", "b") === rlgrep("c", lines("a", "b", "c")))

    assert(lines("b") === rlgrep("a|c", lines("a", "b", "c")))
    assert(lines("c") === rlgrep("b|a", lines("a", "b", "c")))
  }

  @Test
  def excludeOneLineBefore {
    assert(lines("b1", "c1") === rlgrep("a", 1, 0, lines("a1", "b1", "c1")))
    assert(lines("c1") === rlgrep("b", 1, 0, lines("a1", "b1", "c1")))
    assert(lines("a1") === rlgrep("c", 1, 0, lines("a1", "b1", "c1")))

    assert(lines("b1", "c1") === rlgrep("a", 1, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "c1") === rlgrep("b", 1, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2") === rlgrep("c", 1, 0, lines("a1", "a2", "b1", "c1")))

    assert(lines("b1", "b2", "c1") === rlgrep("a", 1, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("c1") === rlgrep("b", 1, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1") === rlgrep("c", 1, 0, lines("a1", "b1", "b2", "c1")))

    assert(lines("b1", "c1", "c2") === rlgrep("a", 1, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("c1", "c2") === rlgrep("b", 1, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1") === rlgrep("c", 1, 0, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("b1", "b2", "b3", "c1", "c2", "c3") === rlgrep("a", 1, 0, in3x3))
    assert(lines("a1", "a2", "c1", "c2", "c3") === rlgrep("b", 1, 0, in3x3))
    assert(lines("a1", "a2", "a3", "b1", "b2") === rlgrep("c", 1, 0, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("b1", "c1", "b2", "c3") === rlgrep("a", 1, 0, in3xMixed))
    assert(lines("c2", "a2", "a3") === rlgrep("b", 1, 0, in3xMixed))
    assert(lines("a1", "b3", "a3") === rlgrep("c", 1, 0, in3xMixed))
  }

  @Test
  def excludeTwoLinesBefore {
    assert(lines("b1", "c1") === rlgrep("a", 2, 0, lines("a1", "b1", "c1")))
    assert(lines("c1") === rlgrep("b", 2, 0, lines("a1", "b1", "c1")))
    assert(lines() === rlgrep("c", 2, 0, lines("a1", "b1", "c1")))

    assert(lines("b1", "c1") === rlgrep("a", 2, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("c1") === rlgrep("b", 2, 0, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1") === rlgrep("c", 2, 0, lines("a1", "a2", "b1", "c1")))

    assert(lines("b1", "b2", "c1") === rlgrep("a", 2, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("c1") === rlgrep("b", 2, 0, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1") === rlgrep("c", 2, 0, lines("a1", "b1", "b2", "c1")))

    assert(lines("b1", "c1", "c2") === rlgrep("a", 2, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines("c1", "c2") === rlgrep("b", 2, 0, lines("a1", "b1", "c1", "c2")))
    assert(lines() === rlgrep("c", 2, 0, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("b1", "b2", "b3", "c1", "c2", "c3") === rlgrep("a", 2, 0, in3x3))
    assert(lines("a1", "c1", "c2", "c3") === rlgrep("b", 2, 0, in3x3))
    assert(lines("a1", "a2", "a3", "b1") === rlgrep("c", 2, 0, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("b1", "c1") === rlgrep("a", 2, 0, in3xMixed))
    assert(lines("c2", "a3") === rlgrep("b", 2, 0, in3xMixed))
    assert(lines("b3", "a3") === rlgrep("c", 2, 0, in3xMixed))
  }

  @Test
  def excludeOneLineAfter {
    assert(lines("c1") === rlgrep("a", 0, 1, lines("a1", "b1", "c1")))
    assert(lines("a1") === rlgrep("b", 0, 1, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1") === rlgrep("c", 0, 1, lines("a1", "b1", "c1")))

    assert(lines("c1") === rlgrep("a", 0, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2") === rlgrep("b", 0, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2", "b1") === rlgrep("c", 0, 1, lines("a1", "a2", "b1", "c1")))

    assert(lines("b2", "c1") === rlgrep("a", 0, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1") === rlgrep("b", 0, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1", "b2") === rlgrep("c", 0, 1, lines("a1", "b1", "b2", "c1")))

    assert(lines("c1", "c2") === rlgrep("a", 0, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "c2") === rlgrep("b", 0, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1") === rlgrep("c", 0, 1, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("b2", "b3", "c1", "c2", "c3") === rlgrep("a", 0, 1, in3x3))
    assert(lines("a1", "a2", "a3", "c2", "c3") === rlgrep("b", 0, 1, in3x3))
    assert(lines("a1", "a2", "a3", "b1", "b2", "b3") === rlgrep("c", 0, 1, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("c1", "b2", "c2", "b3") === rlgrep("a", 0, 1, in3xMixed))
    assert(lines("a1", "a2", "c3") === rlgrep("b", 0, 1, in3xMixed))
    assert(lines("a1", "b1", "a3") === rlgrep("c", 0, 1, in3xMixed))
  }

  @Test
  def excludeTwoLinesAfter {
    assert(lines() === rlgrep("a", 0, 2, lines("a1", "b1", "c1")))
    assert(lines("a1") === rlgrep("b", 0, 2, lines("a1", "b1", "c1")))
    assert(lines("a1", "b1") === rlgrep("c", 0, 2, lines("a1", "b1", "c1")))

    assert(lines() === rlgrep("a", 0, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2") === rlgrep("b", 0, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2", "b1") === rlgrep("c", 0, 2, lines("a1", "a2", "b1", "c1")))

    assert(lines("c1") === rlgrep("a", 0, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1") === rlgrep("b", 0, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1", "b2") === rlgrep("c", 0, 2, lines("a1", "b1", "b2", "c1")))

    assert(lines("c2") === rlgrep("a", 0, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1") === rlgrep("b", 0, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1", "b1") === rlgrep("c", 0, 2, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("b3", "c1", "c2", "c3") === rlgrep("a", 0, 2, in3x3))
    assert(lines("a1", "a2", "a3", "c3") === rlgrep("b", 0, 2, in3x3))
    assert(lines("a1", "a2", "a3", "b1", "b2", "b3") === rlgrep("c", 0, 2, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("b2", "c2") === rlgrep("a", 0, 2, in3xMixed))
    assert(lines("a1", "c3") === rlgrep("b", 0, 2, in3xMixed))
    assert(lines("a1", "b1") === rlgrep("c", 0, 2, in3xMixed))
  }

  @Test
  def excludeOneLineAround {
    assert(lines("c1") === rlgrep("a", 1, 1, lines("a1", "b1", "c1")))
    assert(lines() === rlgrep("b", 1, 1, lines("a1", "b1", "c1")))
    assert(lines("a1") === rlgrep("c", 1, 1, lines("a1", "b1", "c1")))

    assert(lines("c1") === rlgrep("a", 1, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1") === rlgrep("b", 1, 1, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1", "a2") === rlgrep("c", 1, 1, lines("a1", "a2", "b1", "c1")))

    assert(lines("b2", "c1") === rlgrep("a", 1, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines() === rlgrep("b", 1, 1, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1", "b1") === rlgrep("c", 1, 1, lines("a1", "b1", "b2", "c1")))

    assert(lines("c1", "c2") === rlgrep("a", 1, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("c2") === rlgrep("b", 1, 1, lines("a1", "b1", "c1", "c2")))
    assert(lines("a1") === rlgrep("c", 1, 1, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("b2", "b3", "c1", "c2", "c3") === rlgrep("a", 1, 1, in3x3))
    assert(lines("a1", "a2", "c2", "c3") === rlgrep("b", 1, 1, in3x3))
    assert(lines("a1", "a2", "a3", "b1", "b2") === rlgrep("c", 1, 1, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines("c1", "b2") === rlgrep("a", 1, 1, in3xMixed))
    assert(lines("a2") === rlgrep("b", 1, 1, in3xMixed))
    assert(lines("a1", "a3") === rlgrep("c", 1, 1, in3xMixed))
  }

  @Test
  def excludeTwoLinesAround {
    assert(lines() === rlgrep("a", 2, 2, lines("a1", "b1", "c1")))
    assert(lines() === rlgrep("b", 2, 2, lines("a1", "b1", "c1")))
    assert(lines() === rlgrep("c", 2, 2, lines("a1", "b1", "c1")))

    assert(lines() === rlgrep("a", 2, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines() === rlgrep("b", 2, 2, lines("a1", "a2", "b1", "c1")))
    assert(lines("a1") === rlgrep("c", 2, 2, lines("a1", "a2", "b1", "c1")))

    assert(lines("c1") === rlgrep("a", 2, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines() === rlgrep("b", 2, 2, lines("a1", "b1", "b2", "c1")))
    assert(lines("a1") === rlgrep("c", 2, 2, lines("a1", "b1", "b2", "c1")))

    assert(lines("c2") === rlgrep("a", 2, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines() === rlgrep("b", 2, 2, lines("a1", "b1", "c1", "c2")))
    assert(lines() === rlgrep("c", 2, 2, lines("a1", "b1", "c1", "c2")))

    val in3x3 = List("a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3")
    assert(lines("b3", "c1", "c2", "c3") === rlgrep("a", 2, 2, in3x3))
    assert(lines("a1", "c3") === rlgrep("b", 2, 2, in3x3))
    assert(lines("a1", "a2", "a3", "b1") === rlgrep("c", 2, 2, in3x3))

    val in3xMixed = List("a1", "b1", "c1", "b2", "c2", "a2", "c3", "b3", "a3")
    assert(lines() === rlgrep("a", 2, 2, in3xMixed))
    assert(lines() === rlgrep("b", 2, 2, in3xMixed))
    assert(lines() === rlgrep("c", 2, 2, in3xMixed))
  }
}