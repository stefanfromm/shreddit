package de.fromm.shreddit.shredders

import org.scalatest.Suite

import de.fromm.shreddit.pipe.Pipe

import org.junit.Test

/**
 * docme: RewriteTest
 *
 * @author Stefan Fromm
 */
class RewriteTest extends Suite with ShredderSuite {
  private def rewrite(detectionPattern: String, replacePattern: String, replacement: String, inLines: List[String]): List[String] =
    {
      val out = newTempFile(lines())
      new Pipe("test", List(newTempFile(inLines)), out).addStep("#1", new Rewrite(Some(detectionPattern), replacePattern, Some(replacement))).run
      loadTextFile(out)
    }

  @Test
  def rewrite {
    assert(lines("a'") === rewrite("a", "a", "a'", lines("a")))
    assert(lines("a") === rewrite("a", "(a)", "a\n", lines("a")))
    assert(lines("a", "b") === rewrite("a", "(a)", "a\n", lines("a", "b")))
    assert(lines("a", "c", "b") === rewrite("a", "a", "a\nc", lines("a", "b")))
    assert(lines("a", "a") === rewrite("a", "a", "a\n$0", lines("a")))
    assert(lines("a", "a") === rewrite("a", "(a)", "a\n$1", lines("a")))
  }

  @Test
  def springDmServiceReferences {
    val line =
      "2014-02-28 08:46:56,146 [ERROR] -  -  - Thread[Timer-0] - org.springframework.osgi.extender.internal.dependencies.startup.DependencyWaiterApplicationContextExecutor - Unable to create application context for [de.di.lvs20.lvs-tte-wsCommunication], unsatisfied dependencies: Dependency on [(objectClass=de.di.lvs20.tte.api.interfaces.service.masterdata.IArticleService)] (from bean [&imported.tte.ArticleService]), Dependency on [(objectClass=de.di.lvs20.tte.api.interfaces.controller.operative.IStockController)] (from bean [&imported.tte.StockController]), Dependency on [(objectClass=de.di.lvs20.tte.api.interfaces.controller.masterdata.IPartnerController)] (from bean [&imported.tte.PartnerController]), Dependency on [(objectClass=de.di.lvs20.tte.api.interfaces.controller.masterdata.IShipperController)] (from bean [&imported.tte.ShipperController]), Dependency on [(objectClass=de.di.lvs20.tte.api.interfaces.service.operative.ITrustcenterService)] (from bean [&imported.tte.TrustcenterService]), Dependency on [(objectClass=de.di.lvs20.tte.api.interfaces.old.ITTEProcessHandler)] (from bean [&imported.tte.processHandler])"
    val rewritten =
      rewrite(
        "DependencyWaiterApplicationContextExecutor - .*unsatisfied dependencies",
        "Dependency on \\[(.+?)\\] \\(from bean \\[(.+?)\\]\\)",
        "\n$1 [$2]",
        lines(line))
    assert(7 === rewritten.length)
    assert("2014-02-28 08:46:56,146 [ERROR] -  -  - Thread[Timer-0] - org.springframework.osgi.extender.internal.dependencies.startup.DependencyWaiterApplicationContextExecutor - Unable to create application context for [de.di.lvs20.lvs-tte-wsCommunication], unsatisfied dependencies: " ===
      rewritten(0))
    assert("(objectClass=de.di.lvs20.tte.api.interfaces.service.masterdata.IArticleService) [&imported.tte.ArticleService], " ===
      rewritten(1))
    assert("(objectClass=de.di.lvs20.tte.api.interfaces.controller.operative.IStockController) [&imported.tte.StockController], " ===
      rewritten(2))
    assert("(objectClass=de.di.lvs20.tte.api.interfaces.controller.masterdata.IPartnerController) [&imported.tte.PartnerController], " ===
      rewritten(3))
    assert("(objectClass=de.di.lvs20.tte.api.interfaces.controller.masterdata.IShipperController) [&imported.tte.ShipperController], " ===
      rewritten(4))
    assert("(objectClass=de.di.lvs20.tte.api.interfaces.service.operative.ITrustcenterService) [&imported.tte.TrustcenterService], " ===
      rewritten(5))
    assert("(objectClass=de.di.lvs20.tte.api.interfaces.old.ITTEProcessHandler) [&imported.tte.processHandler]" ===
      rewritten(6))
  }
}
