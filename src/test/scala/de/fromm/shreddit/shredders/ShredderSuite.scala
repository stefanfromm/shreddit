package de.fromm.shreddit.shredders

import de.fromm.shreddit.io.TextFile
import org.junit.After
import java.io.File
import de.fromm.shreddit.io.OpenMode
import de.fromm.shreddit.lines.Line
import de.fromm.shreddit.lines.LineBreak
import org.scalatest.Suite
import de.fromm.shreddit.io.TextFile

/**
 * docme: ShredderSuite
 *
 * @author Stefan Fromm
 */
trait ShredderSuite extends Suite {
  private var _tempFiles = List.empty[File]

  @After
  final def cleanTempFiles {
    for (current <- _tempFiles)
      assert(true === current.delete)
  }

  protected final def newTempFile(content: List[String]): TextFile = {
    val tempFile = File.createTempFile(classOf[LineCounterTest].getName, ".in")
    _tempFiles = _tempFiles ++ List(tempFile)

    val textFile = new TextFile(tempFile.getPath, "utf-8")
    val out = textFile.openForWriting(OpenMode.Replace)
    try {
      for (line <- content)
        out.write(new Line {
          def content = line
          def lineBreak = LineBreak.Auto //todo: use LineBreak.None for last line
        })
    } finally {
      out.close
    }
    textFile
  }

  protected final def loadTextFile(in: TextFile): List[String] = {
    var lines = List.empty[String]
    val lineIn = in.openForReading
    try {
      var line = lineIn.read
      while (line.isDefined) {
        lines = lines ++ List(line.get.content)
        line = lineIn.read
      }
    } finally {
      lineIn.close
    }
    lines
  }

  protected def lines(lines: String*) =
    lines.toList
}