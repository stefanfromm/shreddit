package de.fromm.shreddit.shredders

import org.junit.Test
import org.scalatest.Suite

import de.fromm.shreddit.pipe.Pipe

/**
 * docme: CorrelateTest
 *
 * @author Stefan Fromm
 */
class CorrelateTest extends Suite with ShredderSuite {
  private val NON_CORRELATED_INTRO = List(
    "",
    "!!! Found start entries without correlated ending or canceling entries !!!",
    "")

  private def correlate(startPattern: String, corrPattern: String, inLines: List[String]): List[String] =
    {
      correlate(startPattern, corrPattern, false, inLines)
    }

  private def correlate(startPattern: String, corrPattern: String, warnNonCorrelated: Boolean, inLines: List[String]) =
    {
      val out = newTempFile(lines())
      new Pipe("test", List(newTempFile(inLines)), out).addStep("#1", new Correlate(startPattern, corrPattern, None, None, None, warnNonCorrelated, false)).run
      loadTextFile(out)
    }

  private def uncorrelated(entries: String*): List[String] =
    return NON_CORRELATED_INTRO ++ entries

  @Test
  def simple {
    assert(Nil === correlate("start", "end", Nil))
    assert(Nil === correlate("start", "end", List("start")))
    assert(Nil === correlate("start", "end", List("a")))
    assert(Nil === correlate("start", "end", List("a", "b")))
    assert(Nil === correlate("start", "end", List("a", "b", "c")))
    assert(List("start", "end") === correlate("start", "end", List("start", "end")))
    assert(List("start", "end") === correlate("start", "end", List("start", "a", "end")))
    assert(List("start", "end") === correlate("start", "end", List("start", "end", "a")))
    assert(List("start", "end") === correlate("start", "end", List("a", "start", "end")))
    assert(
      List("1 start", "3 end", "2 start", "4 end") ===
        correlate("start", "end", List("1 start", "2 start", "3 end", "4 end")))
    assert(
      List("2 start", "3 end") ===
        correlate("start", "end", List("1 end", "2 start", "3 end")))
    assert(
      List("a start", "an end") ===
        correlate("start", "end", List("a start", "an end")))
    assert(
      List("start", "end") ===
        correlate("start", "end", List("start", "end", "start")))
    assert(
      List("start", "end") ===
        correlate("start", "end", List("start", "end", "end")))
    assert(
      List("start", "end") ===
        correlate("start", "end", List("start", "end", "end", "start")))
    assert(
      List("start", "end", "start", "end") ===
        correlate("start", "end", List("start", "end", "end", "start", "end")))
  }

  @Test
  def warnNonCorrelated {
    assert(Nil === correlate("start", "end", true, Nil))
    assert(uncorrelated("start") === correlate("start", "end", true, List("start")))
    assert(Nil === correlate("start", "end", true, List("a")))
    assert(Nil === correlate("start", "end", true, List("a", "b")))
    assert(Nil === correlate("start", "end", true, List("a", "b", "c")))
    assert(List("start", "end") === correlate("start", "end", true, List("start", "end")))
    assert(
      List("start", "end") ===
        correlate("start", "end", true, List("start", "a", "end")))
    assert(
      List("start", "end") ===
        correlate("start", "end", true, List("start", "end", "a")))
    assert(
      List("start", "end") ===
        correlate("start", "end", true, List("a", "start", "end")))
    assert(
      List("1 start", "3 end", "2 start", "4 end") ===
        correlate("start", "end", true, List("1 start", "2 start", "3 end", "4 end")))
    assert(
      List("2 start", "3 end") ===
        correlate("start", "end", true, List("1 end", "2 start", "3 end")))
    assert(
      List("a start", "an end") ===
        correlate("start", "end", true, List("a start", "an end")))
    assert(
      List("start", "end") ++ uncorrelated("start") ===
        correlate("start", "end", true, List("start", "end", "start")))
    assert(
      List("start", "end") ===
        correlate("start", "end", true, List("start", "end", "end")))
    assert(
      List("start", "end") ++ uncorrelated("start") ===
        correlate("start", "end", true, List("start", "end", "end", "start")))
    assert(
      List("start", "end", "start", "end") ===
        correlate("start", "end", true, List("start", "end", "end", "start", "end")))
  }

  @Test
  def dollarReferences {
    assert(
      List("1 HttpGETRequest", "2 HttpGETResponse", "3 HttpPUTRequest", "4 HttpPUTResponse") ===
        correlate("Http(GET|PUT|POST)Request", "Http$1Response", List(
          "1 HttpGETRequest",
          "2 HttpGETResponse",
          "3 HttpPUTRequest",
          "4 HttpPUTResponse")))

    assert(
      List("1 HttpGETRequest", "3 HttpGETResponse", "2 HttpPUTRequest", "4 HttpPUTResponse") ===
        correlate("Http(GET|PUT|POST)Request", "Http$1Response", List(
          "1 HttpGETRequest",
          "2 HttpPUTRequest",
          "3 HttpGETResponse",
          "4 HttpPUTResponse")))

    assert(
      List("2 HttpPUTRequest", "3 HttpPUTResponse", "1 HttpGETRequest", "4 HttpGETResponse") ===
        correlate("Http(GET|PUT|POST)Request", "Http$1Response", List(
          "1 HttpGETRequest",
          "2 HttpPUTRequest",
          "3 HttpPUTResponse",
          "4 HttpGETResponse")))

    assert(
      List("1 HttpGETRequest", "4 HttpGETResponse") ===
        correlate("Http(GET|PUT|POST)Request", "Http$1Response", List(
          "1 HttpGETRequest",
          "2 HttpPUTResponse",
          "3 HttpPUTRequest",
          "4 HttpGETResponse")))
  }
}