package de.fromm.shreddit.cli

import org.scalatest.Suite
import org.junit.Test

/**
 * docme: ConstTest
 *
 * @author Stefan Fromm
 */
class ConstTest extends Suite {
  private def eval(value: String, replacements: Map[String, String]): String =
    ConstVal.eval(value, replacements)

  @Test
  def eval {
    assert("${}" === eval("${}", Map("x" -> "1")))
    assert("1" === eval("${x}", Map("x" -> "1")))
    assert("21" === eval("2${x}", Map("x" -> "1")))
    assert("13" === eval("${x}3", Map("x" -> "1")))
    assert("${y}" === eval("${y}", Map("x" -> "1")))
    assert("${x${y}}" === eval("${x${y}}", Map("x" -> "1")))
    assert("${x2}" === eval("${x${y}}", Map("x" -> "1", "y" -> "2")))
    assert("1" === eval("${x${y}}", Map("x" -> "1", "y" -> "")))
    assert("1" === eval("${x}", Map("x" -> "${y}", "y" -> "1")))
    assert("21" === eval("${x}", Map("x" -> "2${y}", "y" -> "1")))
    assert("13" === eval("${x}", Map("x" -> "${y}3", "y" -> "1")))
    assert("213" === eval("${x}", Map("x" -> "2${y}3", "y" -> "1")))
    assert("${x}" === eval("""\$\{x\}""", Map("x" -> "1")))
    assert("${x1}" === eval("""\$\{x${x}\}""", Map("x" -> "1")))
    assert("${x${x}}" === eval("""\$\{x\$\{x\}\}""", Map("x" -> "1")))
    assert("1" === eval("${${x}}", Map("x" -> "y", "y" -> "1")))
    assert("${${x}}" === eval("""${\$\{x\}}""", Map("x" -> "y", "y" -> "1")))
    assert("y" === eval("""${\$\{x\}}""", Map("${x}" -> "y", "y" -> "1")))
    assert("1" === eval("""${${\$\{x\}}}""", Map("${x}" -> "y", "y" -> "1")))
    assert("""\\\\\w+\\.+""" === eval("""\\\\\\\\\\w+\\\\.+""", Map()))
  }
}