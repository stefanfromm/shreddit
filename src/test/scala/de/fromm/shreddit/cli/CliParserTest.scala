package de.fromm.shreddit.cli

import org.scalatest.Suite
import org.junit.Test

/**
 * docme: CliParserTest
 *
 * @author Stefan Fromm
 */
class CliParserTest extends Suite with CliParser {
  @Test
  def argRegex {
    val arg = """(?:"[^"]*(?:""[^"]*)*")|[^\s"]+""".r

    def matches(s: String) =
      arg.pattern.matcher(s).matches

    assert(!matches(""))
    assert(!matches(" "))
    assert(!matches("  "))
    assert(matches("a"))
    assert(matches("1"))
    assert(matches("\"a\""))
    assert(matches("\" a\""))
    assert(matches("\"a \""))
    assert(matches("\" a \""))
    assert(matches("\"  a\""))
    assert(matches("\"a  \""))
    assert(matches("\"  a  \""))
    assert(matches("\"1\""))
    assert(matches("\" 1\""))
    assert(matches("\"1 \""))
    assert(matches("\" 1 \""))
    assert(matches("\"  1\""))
    assert(matches("\"1  \""))
    assert(matches("\"  1  \""))
    assert(!matches("\""))
    assert(matches("\"\""))
    assert(matches("\" \""))
  }

  @Test
  def argRegexWithStopper {
    val arg = """(?:(?:"[^"]*(?:""[^"]*)*")|[^\s"]+)(?<!\Q->\E)""".r

    def matches(s: String) =
      arg.pattern.matcher(s).matches

    assert(matches("01.log"))
    assert(!matches("01.log "))
    assert(!matches("01.log->"))
    assert(!matches("01.log ->"))
  }

  @Test
  def argRegexWithBlocker {
    val arg = """(?!\Q->\E)(?:(?:"[^"]*(?:""[^"]*)*")|[^\s"]+)""".r

    def matches(s: String) =
      arg.pattern.matcher(s).matches

    assert(matches("01.log"))
    assert(!matches(" 01.log"))
    assert(!matches("->01.log"))
    assert(!matches("-> 01.log"))
  }

  private def parseCmdLine(cmdLine: String) = {
    parseAll(command, cmdLine)
  }

  @Test
  def cmdLineEmpty {
    val res = parseCmdLine("")
    assert(false === res.successful)
    assert(true === res.isEmpty)
    val failure = res.asInstanceOf[Failure]
    assert("No valid command." === failure.msg)
    assert(0 === failure.next.offset)
    assert(1 === failure.next.pos.column)
    assert(1 === failure.next.pos.line)
  }

  @Test
  def cmdLineMissingArg1 {
    val res = parseCmdLine("cd")
    assert(false === res.successful)
    assert(true === res.isEmpty)
    val failure = res.asInstanceOf[Failure]
    assert("No working directory." === failure.msg)
    assert(2 === failure.next.offset)
    assert(3 === failure.next.pos.column)
    assert(1 === failure.next.pos.line)
  }

  @Test
  def cmdLineMissingArg2 {
    val res = parseCmdLine("cd ")
    assert(false === res.successful)
    assert(true === res.isEmpty)
    val failure = res.asInstanceOf[Failure]
    assert("No working directory." === failure.msg)
    assert(2 === failure.next.offset)
    assert(3 === failure.next.pos.column)
    assert(1 === failure.next.pos.line)
  }

  @Test
  def cmdLineMissingArg3 {
    val res = parseCmdLine("cd \"")
    assert(false === res.successful)
    assert(true === res.isEmpty)
    val failure = res.asInstanceOf[Failure]
    assert("No working directory." === failure.msg)
    assert(2 === failure.next.offset)
    assert(3 === failure.next.pos.column)
    assert(1 === failure.next.pos.line)
  }

  @Test
  def cmdLineMissingArg4 {
    val res = parseCmdLine("cd \" ")
    assert(false === res.successful)
    assert(true === res.isEmpty)
    val failure = res.asInstanceOf[Failure]
    assert("No working directory." === failure.msg)
    assert(2 === failure.next.offset)
    assert(3 === failure.next.pos.column)
    assert(1 === failure.next.pos.line)
  }
}